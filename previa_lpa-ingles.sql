-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 01/02/2018 às 14:20
-- Versão do servidor: 8.0.2-dmr
-- Versão do PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `lpa`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1_legenda` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1_legenda_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2_legenda` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2_legenda_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `blog`
--

INSERT INTO `blog` (`id`, `data`, `titulo`, `titulo_en`, `texto`, `texto_en`, `imagem_1`, `imagem_1_legenda`, `imagem_1_legenda_en`, `imagem_2`, `imagem_2_legenda`, `imagem_2_legenda_en`, `created_at`, `updated_at`) VALUES
(1, '2018-01-30', 'teste', '.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum faucibus sem, at porta sapien sollicitudin vel. Vivamus ultricies dui nec arcu congue mattis. Quisque dictum nunc est, vel imperdiet nulla tristique sed. Maecenas non odio erat. Nunc diam mauris, varius eu malesuada in, egestas vitae eros. Sed elementum, magna sit amet convallis mollis, ligula enim tincidunt purus, ut porta dolor dolor at ligula. Donec scelerisque faucibus fermentum. Donec at rhoncus sem. Donec sed urna imperdiet, tristique odio consequat, finibus libero.</p>\r\n', '<p>.</p>\r\n', 'arquiteturacomercial_20180130132445.jpg', 'teste postagem', '.', '', '', '', '2018-01-30 15:24:53', '2018-02-01 14:19:59'),
(2, '2018-01-30', 'TESTE', '.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum faucibus sem, at porta sapien sollicitudin vel. Vivamus ultricies dui nec arcu congue mattis. Quisque dictum nunc est, vel imperdiet nulla tristique sed. Maecenas non odio erat. Nunc diam mauris, varius eu malesuada in, egestas vitae eros. Sed elementum, magna sit amet convallis mollis, ligula enim tincidunt purus, ut porta dolor dolor at ligula. Donec scelerisque faucibus fermentum. Donec at rhoncus sem. Donec sed urna imperdiet, tristique odio consequat, finibus libero.</p>\r\n', '<p>.</p>\r\n', 'arquiteturacorporativa_20180130132651.jpg', '', '', '', '', '', '2018-01-30 15:26:52', '2018-02-01 14:19:55');

-- --------------------------------------------------------

--
-- Estrutura para tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `clientes`
--

INSERT INTO `clientes` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, '57-under-armour.png', NULL, NULL),
(2, 0, '56-toyota.png', NULL, NULL),
(3, 0, '55-temasek.png', NULL, NULL),
(4, 0, '54-sum-up.png', NULL, NULL),
(5, 0, '53-center-norte.png', NULL, NULL),
(6, 0, '52-reunion.png', NULL, NULL),
(7, 0, '51-reinaldo-lourenco.png', NULL, NULL),
(8, 0, '50-redhat.png', NULL, NULL),
(9, 0, '49-promove.png', NULL, NULL),
(10, 0, '48-pontal.png', NULL, NULL),
(11, 0, '47-pandora.png', NULL, NULL),
(12, 0, '46-netza.png', NULL, NULL),
(13, 0, '45-gordilho.png', NULL, NULL),
(14, 0, '44-nespresso.png', NULL, NULL),
(15, 0, '43-whirlpool.png', NULL, NULL),
(16, 0, '42-moinhos-cruzeiro-do-sul.png', NULL, NULL),
(17, 0, '41-msquare.png', NULL, NULL),
(18, 0, '40-manroland.png', NULL, NULL),
(19, 0, '39-licon-advogados.png', NULL, NULL),
(20, 0, '38-lindencorp.png', NULL, NULL),
(21, 0, '37-lar-center.png', NULL, NULL),
(22, 0, '36-l\'espace.png', NULL, NULL),
(23, 0, '35-interbrand.png', NULL, NULL),
(24, 0, '34-incoplast.png', NULL, NULL),
(25, 0, '33-hsbc.png', NULL, NULL),
(26, 0, '32-hansgrohe.png', NULL, NULL),
(27, 0, '31-gustavo-padilha.png', NULL, NULL),
(28, 0, '30-semco.png', NULL, NULL),
(29, 0, '29-ldi.png', NULL, NULL),
(30, 0, '28-durocolor.png', NULL, NULL),
(31, 0, '27-glencore.png', NULL, NULL),
(32, 0, '26-gavilon.png', NULL, NULL),
(33, 0, '25-porto-das-artes.png', NULL, NULL),
(34, 0, '24-mataboi.png', NULL, NULL),
(35, 0, '23-finivest.png', NULL, NULL),
(36, 0, '22-fecomercio.png', NULL, NULL),
(37, 0, '21-ebac.png', NULL, NULL),
(38, 0, '20-dentsu.png', NULL, NULL),
(39, 0, '19-dolce-gusto.png', NULL, NULL),
(40, 0, '18-delfim.png', NULL, NULL),
(41, 0, '17-cris-barros.png', NULL, NULL),
(42, 0, '16-correcta.png', NULL, NULL),
(43, 0, '15-adolpho.png', NULL, NULL),
(44, 0, '14-cipasa.png', NULL, NULL),
(45, 0, '13-cegedim.png', NULL, NULL),
(46, 0, '12-brampac.png', NULL, NULL),
(47, 0, '11-bva.png', NULL, NULL),
(48, 0, '10-blockbuster.png', NULL, NULL),
(49, 0, '09-b.a.m&a.png', NULL, NULL),
(50, 0, '08-bain.png', NULL, NULL),
(51, 0, '07-ferla.png', NULL, NULL),
(52, 0, '06-vanessa.png', NULL, NULL),
(53, 0, '05-abf.png', NULL, NULL),
(54, 0, '04-anfab.png', NULL, NULL),
(55, 0, '03-andrioli.png', NULL, NULL),
(56, 0, '02-amcor.png', NULL, NULL),
(57, 0, '01-almaviva.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome_do_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `nome_do_site`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'LPA Arquitetura', 'LPA Arquitetura', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_en` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `endereco_en`, `google_maps`, `facebook`, `instagram`, `linkedin`, `youtube`, `pinterest`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '11 3815.9393', 'rua pedroso de alvarenga, 1.254 &bull; 10&ordm; andar &bull; cj. 102<br />\r\nitaim bibi &bull; S&atilde;o Paulo sp &bull; cep 04531-004', '.', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4348.456267448668!2d-46.68417070099687!3d-23.582759245515007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce576834fc89c5%3A0x9ff2bba372dc3a82!2sR.+Pedroso+Alvarenga%2C+1254+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04531-004!5e0!3m2!1spt-BR!2sbr!4v1517319068732\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'https://www.facebook.com/pages/Leonetti-Piemonte-Arquitetura/135094070013323?fref=ts', 'instagram', 'linkedin', 'youtube', 'https://br.pinterest.com/LPArquitetura/', NULL, '2018-02-01 14:20:03');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `depoimentos`
--

CREATE TABLE `depoimentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `depoimentos`
--

INSERT INTO `depoimentos` (`id`, `ordem`, `titulo`, `titulo_en`, `subtitulo`, `subtitulo_en`, `texto`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, 1, 'BRW Investimentos ', '.', 'Mário Brunido (sócio)', '.', '&ldquo;A experi&ecirc;ncia da BRW com a LPA foi no nosso ponto de vista, muito boa. O nosso escrit&oacute;rio ficou exatamente do jeito que gostar&iacute;amos e o budget original foi respeitado. Certamente recomendaria e voltaria a contratar a LPA! &rdquo;', '.', '2018-01-30 14:41:24', '2018-02-01 14:18:13'),
(2, 2, 'Carpa Patrimonial', '.', 'Pedro Romeiro (sócio)', '.', '&ldquo;Os servi&ccedil;os prestados pela Leonetti Piemonte Arquitetura tiveram como resultado um elevado n&iacute;vel de qualidade. A equipe &eacute; atenciosa, comprometida e possui alta capacidade t&eacute;cnica. Tanto o projeto quanto a condu&ccedil;&atilde;o dos demais envolvidos no processo atenderam nossas expectativas. Nos sentimos confort&aacute;veis em indicar e fechar novos neg&oacute;cios com a LPA. &rdquo;', '.', '2018-01-30 14:41:54', '2018-02-01 14:18:24'),
(3, 3, 'Bain & Company', '.', 'Jean-Claude Ramirez (sócio)', '.', '&ldquo;N&oacute;s trabalhamos com a Leonetti Piemonte Arquitetura por muito tempo, desde a chegada da Bain &amp; Company no Brasil. No nosso mais novo escrit&oacute;rio em S&atilde;o Paulo, decidimos criar um escrit&oacute;rio muito mais aberto e din&acirc;mico, proporcionando um espa&ccedil;o de trabalho menos conservador. A LPA atingiu nossas expectativas com excel&ecirc;ncia, criando uma mistura de espa&ccedil;os abertos e fechados, com &aacute;reas colaborativas e salas din&acirc;micas. Acreditamos que nosso escrit&oacute;rio &eacute; realmente um &oacute;timo lugar para trabalhar, e parabenizamos a equipe da LPA, bem como a construtora e fornecedores pelo &oacute;timo trabalho. &rdquo;', '.', '2018-01-30 14:42:19', '2018-02-01 14:18:32'),
(4, 4, 'Escola Britânica de Artes Criativas', '.', 'Maurício Tortosa (c.e.o)', '.', '&ldquo;Nossa rela&ccedil;&atilde;o com a Leonetti Piemonte Arquitetura envolveu o desafio de tempo, qualidade e di&aacute;logo com as demais equipes de arquitetura e a construtora. Nunca conseguir&iacute;amos abrir a EBAC a tempo, com todas as barreiras no caminho, se n&atilde;o fosse pelo profissionalismo e dedica&ccedil;&atilde;o da LPA. Tamb&eacute;m n&atilde;o conseguir&iacute;amos conhecer e negociar com todos os fornecedores sem o aux&iacute;lio da LPA. A EBAC foi um milagre. Serei eternamente grato, pois al&eacute;m de profissionais que recomendo a quem for, ganhei amigas para a vida toda. &rdquo;', '.', '2018-01-30 14:42:47', '2018-02-01 14:18:41'),
(5, 5, 'Multinacional de Agronegócios', '.', '.', '.', '&quot;A LPA nos atende sempre com alta compreens&atilde;o e sensibilidade com rela&ccedil;&atilde;o ao que queremos em nossos projetos! Disponibiliza excelentes indica&ccedil;&otilde;es de fornecedores para aquisi&ccedil;&atilde;o de mobili&aacute;rio, divis&oacute;rias, demais instala&ccedil;&otilde;es e servi&ccedil;os, assim como pleno suporte no p&oacute;s-obra. A equipe de execu&ccedil;&atilde;o &eacute; muito eficiente e existe extrema aten&ccedil;&atilde;o ao prazo estipulado e os acabamentos s&atilde;o impec&aacute;veis!&quot;', '.', '2018-01-30 14:43:18', '2018-02-01 14:18:55'),
(6, 6, 'Correcta', '.', 'Márcia Regina Crisosto (diretora administrativa financeira)', '.', '&#39;&#39;A Leonetti Piemonte Arquitetura nos ajudou a resolver um problema bastante antigo que era transformar um im&oacute;vel residencial desativado e localizado num terreno amplamente arborizado na nova sede administrativa da Correcta.<br />\r\nA solu&ccedil;&atilde;o apresentada pela LPA foi um projeto de retrofit que criou espa&ccedil;os internos e externos de conviv&ecirc;ncia e trabalho que garantem excelente qualidade de vida para todos os nossos funcion&aacute;rios.<br />\r\nComo resultado, al&eacute;m da excelente avalia&ccedil;&atilde;o obtida de colaboradores satisfeitos com a qualidade e funcionalidade do ambiente f&iacute;sico, somos constantemente elogiados por todos que visitam nosso ambiente de trabalho.<br />\r\nA criatividade, dedica&ccedil;&atilde;o e profissionalismo da LPA do in&iacute;cio ao fim do projeto e constru&ccedil;&atilde;o foram os motivos do grande sucesso de nosso novo escrit&oacute;rio.&#39;&#39;', '.', '2018-01-30 14:44:16', '2018-02-01 14:19:04'),
(7, 7, 'Under Armour Brasil', '.', 'Daniela Cadena (senior dtc manager)', '.', '&ldquo;Trabalhamos em parceria com a Leonetti Piemonte Arquitetura desde o primeiro ano de opera&ccedil;&atilde;o no Brasil. Escolhemos a LPA como nosso bra&ccedil;o de arquitetura pela seriedade e profissionalismo com que tratam clientes e conduzem projetos. Os servi&ccedil;os realizados para a Under Armour sempre excederam as expectativas nas diversas obras que realizamos em conjunto. O planejamento, projeto e condu&ccedil;&atilde;o da obra foram sempre tratados com alto n&iacute;vel de qualidade, bem como a gest&atilde;o dos demais envolvidos nos processos.<br />\r\nAs obras foram sempre conduzidas com precis&atilde;o e fomos informados detalhadamente do desenvolvimento da mesma. As entregas das obras foram feitas sem transtornos, e a LPA continua nos atendendo em novas solicita&ccedil;&otilde;es e projetos. &rdquo;', '.', '2018-01-30 14:44:46', '2018-02-01 14:19:12');

-- --------------------------------------------------------

--
-- Estrutura para tabela `destaques`
--

CREATE TABLE `destaques` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `projeto` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `destaques`
--

INSERT INTO `destaques` (`id`, `ordem`, `projeto`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '2018-01-30 19:52:11', '2018-01-30 19:52:11'),
(2, 2, 2, '2018-01-30 19:56:14', '2018-01-30 19:56:14'),
(3, 1, 3, '2018-01-30 19:58:33', '2018-01-30 19:58:33'),
(4, 0, 4, '2018-01-30 20:07:45', '2018-01-30 20:07:45');

-- --------------------------------------------------------

--
-- Estrutura para tabela `equipe`
--

CREATE TABLE `equipe` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `equipe`
--

INSERT INTO `equipe` (`id`, `ordem`, `nome`, `imagem`, `cargo`, `cargo_en`, `e_mail`, `created_at`, `updated_at`) VALUES
(1, 1, 'Isabella Leonetti', 'mg-4127_20180130170426.JPG', 'Sócia', '.', 'isabella@lpa.arq.br', '2018-01-30 15:05:23', '2018-02-01 14:19:18'),
(2, 2, 'Pierina Piemonte', 'mg-4160_20180130170527.JPG', 'Sócia', '.', 'pierina@lpa.arq.br', '2018-01-30 15:06:32', '2018-02-01 14:19:21'),
(3, 3, 'Carolina Correia', 'mg-4182_20180130170736.JPG', 'Arquiteta de Projetos', '.', 'carolina@lpa.arq.br', '2018-01-30 15:08:26', '2018-02-01 14:19:23'),
(4, 4, 'Marina Magliocca', 'mg-41011_20180130164922.jpg', 'Arquiteta de Projetos', '.', 'marina@lpa.arq.br', '2018-01-30 15:10:09', '2018-02-01 14:19:26'),
(5, 5, 'Mariane Ortiz', 'mg-4057w_20180130170138.jpg', 'Arquiteta de Projetos', '.', 'mariane@lpa.arq.br', '2018-01-30 15:11:18', '2018-02-01 14:19:28'),
(6, 6, 'Ana Luiza Camargo', 'mg-41244_20180130170206.jpg', 'Engenheira de Gestão', '.', 'analuiza@lpa.arq.br', '2018-01-30 15:12:43', '2018-02-01 14:19:31'),
(7, 7, 'Rafael Boaretto', 'mg-412211_20180130170247.jpg', 'Arquiteto de Gestão', '.', 'rafael@lpa.arq.br', '2018-01-30 15:14:21', '2018-02-01 14:19:34'),
(8, 8, 'Elaine Cortez', 'mg-4099_20180130170923.JPG', 'Administrativo/Financeiro', '.', 'elaine@lpa.arq.br', '2018-01-30 15:17:59', '2018-02-01 14:19:37'),
(9, 9, 'Yesa Moura', 'mg-4087_20180130171048.JPG', 'Administrativo/Financeiro', '.', 'yesa@lpa.arq.br', '2018-01-30 15:19:05', '2018-02-01 14:19:40'),
(10, 10, 'Lilian Ferreira', 'mg-4067_20180130171452.JPG', 'Representante Comercial', '.', 'lilian@lpa.arq.br', '2018-01-30 15:22:25', '2018-02-01 14:19:42'),
(11, 11, 'Marcello Delai', 'mg-4170_20180130171343.JPG', 'Arquiteto de Projetos / Marketing', '.', 'marcello@lpa.arq.br', '2018-01-30 15:23:29', '2018-02-01 14:19:45'),
(12, 12, 'Ana Clara Di Sessa Vital', 'mg-4151_20180130171436.JPG', 'Estagiária', '.', 'ana@lpa.arq.br', '2018-01-30 15:24:07', '2018-02-01 14:19:48');

-- --------------------------------------------------------

--
-- Estrutura para tabela `equipe_projetos`
--

CREATE TABLE `equipe_projetos` (
  `id` int(10) UNSIGNED NOT NULL,
  `projetos_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `equipe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `equipe_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `escritorio`
--

CREATE TABLE `escritorio` (
  `id` int(10) UNSIGNED NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quem_somos` text COLLATE utf8_unicode_ci NOT NULL,
  `quem_somos_en` text COLLATE utf8_unicode_ci NOT NULL,
  `o_escritorio` text COLLATE utf8_unicode_ci NOT NULL,
  `o_escritorio_en` text COLLATE utf8_unicode_ci NOT NULL,
  `missao` text COLLATE utf8_unicode_ci NOT NULL,
  `missao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `visao` text COLLATE utf8_unicode_ci NOT NULL,
  `visao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `premios` text COLLATE utf8_unicode_ci NOT NULL,
  `premios_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `escritorio`
--

INSERT INTO `escritorio` (`id`, `banner`, `quem_somos`, `quem_somos_en`, `o_escritorio`, `o_escritorio_en`, `missao`, `missao_en`, `visao`, `visao_en`, `premios`, `premios_en`, `created_at`, `updated_at`) VALUES
(1, 'imagem-de-fundo_20180130122440.jpg', '<p>Acreditamos no poder do espa&ccedil;o.<br />\r\nO espa&ccedil;o pode transformar a maneira das pessoas pensarem, sentirem e se relacionarem. N&oacute;s podemos ajudar a criar ambientes que adotam conforto, colabora&ccedil;&atilde;o e aconchego.<br />\r\nO processo tem in&iacute;cio AQUI NA LPA.<br />\r\n&nbsp;</p>\r\n', '<p>.</p>\r\n', '<p>A LPA v&ecirc; o design como estrat&eacute;gia em a&ccedil;&atilde;o, focada nos resultados. Ajudamos nossos clientes a imaginar um futuro melhor e chegar com sucesso. A LPA acredita no poder do projeto para gerar solu&ccedil;&otilde;es inovadoras que afetam a transforma&ccedil;&atilde;o real.</p>\r\n\r\n<p>Os projetos da LPA s&atilde;o executados com precis&atilde;o e cumprimos os compromissos firmados com nossos clientes na fase inicial do processo: prazo, or&ccedil;amento, inova&ccedil;&atilde;o e qualidade.<br />\r\nCom transpar&ecirc;ncia, &eacute;tica e responsabilidades, estamos h&aacute; mais de 15 anos no mercado.</p>\r\n\r\n<p>Estes s&atilde;o os conceitos que regem o trabalho da LPA Arquitetura. Isabella Leonetti e Pierina Piemonte s&atilde;o arquitetas formadas pela Universidade Mackenzie com MBA em gest&atilde;o pela Funda&ccedil;&atilde;o Get&uacute;lio Vargas em S&atilde;o Paulo.<br />\r\n&nbsp;</p>\r\n', '<p>.</p>\r\n', '\"Nosso compromisso é atender nossos clientes e conhecer seus desejos para dar forma às suas necessidades desenvolvendo projetos inovadores com excelência em design, otimização de espaço, pleno conhecimento em tecnologias e sensibilidade para custos e prazos. “', '.', '\"Ser reconhecido como um escritório especializado em arquitetura CORPORATIVA e COMERCIAL inovador, transparente, sendo o melhor parceiro na prestação de serviços que excedam as expectativas do cliente.\"', '.', '<p>2017 &ndash; Pr&ecirc;mio Talent Club &ndash; Hunter Douglas<br />\r\n2014 - XI Grande Pr&ecirc;mio de Arquitetura Corporativa &ndash; Nova Sede Correcta<br />\r\n2013 - 3&deg; Lugar no 1&ordm; Pr&ecirc;mio TOP &ndash; Trof&eacute;u de Obras Placo 2013 no segmento Corporativo<br />\r\n2011 - 1&deg; Lugar Concurso Cultural Decora&ccedil;&atilde;o de Banheiros Lorenzetti<br />\r\n2010 - Pr&ecirc;mio Top Empreendedor<br />\r\n2006 - Pr&ecirc;mio &ldquo;Top of Quality&rdquo;<br />\r\n2005 - II Grande Pr&ecirc;mio de Arquitetura Corporativa &ndash; Bain Brasil<br />\r\n&nbsp;</p>\r\n', '<p>.</p>\r\n', NULL, '2018-02-01 14:16:28');

-- --------------------------------------------------------

--
-- Estrutura para tabela `estudos`
--

CREATE TABLE `estudos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `equipe` text COLLATE utf8_unicode_ci NOT NULL,
  `equipe_en` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais_tecnicos` text COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais_tecnicos_en` text COLLATE utf8_unicode_ci NOT NULL,
  `padroes_de_cores` text COLLATE utf8_unicode_ci NOT NULL,
  `padroes_de_cores_en` text COLLATE utf8_unicode_ci NOT NULL,
  `novos_conceitos` text COLLATE utf8_unicode_ci NOT NULL,
  `novos_conceitos_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_10` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `expertise`
--

CREATE TABLE `expertise` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `expertise`
--

INSERT INTO `expertise` (`id`, `ordem`, `titulo`, `titulo_en`, `texto`, `texto_en`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 'ARQUITETURA CORPORATIVA', '.', '<p>A concep&ccedil;&atilde;o de um projeto corporativo deve estar em sintonia com a cultura organizacional, traduzindo sua miss&atilde;o, vis&atilde;o e valores tanto para seu p&uacute;blico interno quanto para seus clientes.<br />\r\n<br />\r\nBuscando o constante alinhamento com a estrat&eacute;gica de comunica&ccedil;&atilde;o da organiza&ccedil;&atilde;o, um projeto organizacional deve focar a busca da maximiza&ccedil;&atilde;o na utiliza&ccedil;&atilde;o das instala&ccedil;&otilde;es e o alinhamento com os modernos conceitos de ergonomia e qualidade de vida no trabalho.&nbsp;<br />\r\n<br />\r\nA proposta da LPA &eacute; empregar as mais recentes tend&ecirc;ncias do Office Design, adequando-os ao perfil de cada cliente, com o objetivo de criar espa&ccedil;os interativos, onde a tecnologia, o espa&ccedil;o e a administra&ccedil;&atilde;o formem uma trilogia essencial na busca dos resultados almejados pela Empresa.<br />\r\n<br />\r\nA arquitetura de interiores &eacute; uma poderosa ferramenta na busca da qualidade, da produtividade e da melhor rela&ccedil;&atilde;o custo benef&iacute;cio nos investimentos.</p>\r\n', '<p>.</p>\r\n', 'arquiteturacorporativa_20180130130033.jpg', '2018-01-30 14:56:45', '2018-02-01 14:17:39'),
(2, 2, 'CONSULTORIA DE PLANEJAMENTO', '.', '<p>Em muitos casos, antes do projeto de arquitetura, h&aacute; a necessidade de um estudo aprofundado sobre a empresa e suas necessidades.<br />\r\n<br />\r\nNeste processo, a LPA aplica a ferramenta de Space Planning &ndash; uma ferramenta metodol&oacute;gica internacionalmente reconhecida para o planejamento de ocupa&ccedil;&atilde;o do espa&ccedil;o.<br />\r\n<br />\r\nPor meio de question&aacute;rios realizados on-line, podemos identificar a situa&ccedil;&atilde;o atual da empresa em quest&otilde;es qualitativas e quantitativas, bem como no seu padr&atilde;o de funcionamento.<br />\r\n<br />\r\nAtrav&eacute;s de um estudo minucioso desta situa&ccedil;&atilde;o, gera-se um diagn&oacute;stico preciso e objetivo que permite a tomada de decis&atilde;o ideal em termos de necessidades funcionais seja na hora de alugar um im&oacute;vel ou na hora de expandir ou reduzir a &aacute;rea ocupada pelo seu escrit&oacute;rio.</p>\r\n', '<p>.</p>\r\n', 'consultoriadeplanejamento_20180130130019.jpg', '2018-01-30 15:00:20', '2018-02-01 14:17:42'),
(3, 3, 'ARQUITETURA COMERCIAL', '.', '<p>A expans&atilde;o cada vez mais r&aacute;pida do com&eacute;rcio na sociedade resulta na necessidade de inovar, investir e construir uma marca fortalecida para sobreviver no mercado competitivo.</p>\r\n\r\n<p>Buscamos traduzir o conceito da marca no espa&ccedil;o, estimulando todos os sentidos e transformando-o em uma ferramenta de marketing ajudando a aumentar as vendas e consolidar a marca.</p>\r\n\r\n<p>Seja na expans&atilde;o e inova&ccedil;&atilde;o de uma marca com presen&ccedil;a no mercado, ou na cria&ccedil;&atilde;o de uma nova arquitetura de acordo com as tend&ecirc;ncias no mercado, o ramo comercial da LPA tem a experi&ecirc;ncia para atender a sua necessidade.</p>\r\n\r\n<p>Realizamos tamb&eacute;m o processo de Nacionaliza&ccedil;&atilde;o, que consiste em traduzir e adequar projetos de marcas internacionais para o mercado, normas e leis brasileiras, auxiliando em etapas cr&iacute;ticas como a escolha do ponto da loja e substitui&ccedil;&atilde;o de especifica&ccedil;&otilde;es importadas por vers&otilde;es nacionais.<br />\r\n&nbsp;</p>\r\n', '<p>.</p>\r\n', 'arquiteturacomercial_20180130130322.jpg', '2018-01-30 15:03:25', '2018-02-01 14:17:46'),
(4, 4, 'ACOMPANHAMENTO DE OBRAS', '.', '<p>Um projeto, por mais perfeito que seja, n&atilde;o vale de nada sem ser executado &agrave; risca e sem um planejamento de custos e prazos realista. Desta forma, a LPA realiza tamb&eacute;m o processo de acompanhamento de obras, de modo a garantir uma boa gest&atilde;o e execu&ccedil;&atilde;o dos projetos realizados.</p>\r\n\r\n<p>A organiza&ccedil;&atilde;o e gest&atilde;o dos terceiros envolvidos para manuten&ccedil;&atilde;o de prazos, an&aacute;lise de or&ccedil;amentos e qualidade na execu&ccedil;&atilde;o &eacute; primordial para a LPA garantir a tranquilidade e bem-estar do cliente.</p>\r\n\r\n<p>Da mesma forma, a LPA supervisiona a entrega de cada obra visando evitar imprevistos e gerir e solucionar quaisquer pend&ecirc;ncias de forma r&aacute;pida e eficiente, dando suporte &agrave;s necessidades do cliente at&eacute; ap&oacute;s o termino da obra.<br />\r\n&nbsp;</p>\r\n', '<p>.</p>\r\n', 'acompanhamentodeobras_20180130130355.jpg', '2018-01-30 15:03:56', '2018-02-01 14:17:49');

-- --------------------------------------------------------

--
-- Estrutura para tabela `fornecedores`
--

CREATE TABLE `fornecedores` (
  `id` int(10) UNSIGNED NOT NULL,
  `projetos_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `material` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detalhes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detalhes_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2018_01_18_143442_create_blog_table', 1),
('2018_01_18_150952_create_equipe_table', 1),
('2018_01_18_151415_create_depoimentos_table', 1),
('2018_01_18_151532_create_clientes_table', 1),
('2018_01_18_151835_create_expertise_table', 1),
('2018_01_18_152155_create_escritorio_table', 1),
('2018_01_22_005517_create_estudos_table', 1),
('2018_01_22_010644_create_projetos_table', 1),
('2018_01_22_232605_create_fornecedores_table', 1),
('2018_01_22_235014_create_equipe_projetos_table', 1),
('2018_01_23_004057_create_destaques_table', 1),
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2018_01_18_143442_create_blog_table', 1),
('2018_01_18_150952_create_equipe_table', 1),
('2018_01_18_151415_create_depoimentos_table', 1),
('2018_01_18_151532_create_clientes_table', 1),
('2018_01_18_151835_create_expertise_table', 1),
('2018_01_18_152155_create_escritorio_table', 1),
('2018_01_22_005517_create_estudos_table', 1),
('2018_01_22_010644_create_projetos_table', 1),
('2018_01_22_232605_create_fornecedores_table', 1),
('2018_01_22_235014_create_equipe_projetos_table', 1),
('2018_01_23_004057_create_destaques_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetos`
--

CREATE TABLE `projetos` (
  `id` int(10) UNSIGNED NOT NULL,
  `projetos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `equipe` text COLLATE utf8_unicode_ci NOT NULL,
  `equipe_en` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais_tecnicos` text COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais_tecnicos_en` text COLLATE utf8_unicode_ci NOT NULL,
  `padroes_de_cores` text COLLATE utf8_unicode_ci NOT NULL,
  `padroes_de_cores_en` text COLLATE utf8_unicode_ci NOT NULL,
  `novos_conceitos` text COLLATE utf8_unicode_ci NOT NULL,
  `novos_conceitos_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_10` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `projetos`
--

INSERT INTO `projetos` (`id`, `projetos_categoria_id`, `ordem`, `slug`, `titulo`, `titulo_en`, `capa`, `cidade`, `cidade_en`, `area`, `area_en`, `ano`, `equipe`, `equipe_en`, `descricao`, `descricao_en`, `diferenciais_tecnicos`, `diferenciais_tecnicos_en`, `padroes_de_cores`, `padroes_de_cores_en`, `novos_conceitos`, `novos_conceitos_en`, `imagem_1`, `imagem_2`, `imagem_3`, `imagem_4`, `imagem_5`, `imagem_6`, `imagem_7`, `imagem_8`, `imagem_9`, `imagem_10`, `created_at`, `updated_at`) VALUES
(1, 2, 0, 'torres-associados', 'Torres Associados', '.', 'dsc2398_20180130172826.jpg', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', 'dsc2289_20180130172827.jpg', 'dsc2306_20180130172828.jpg', 'dsc2347_20180130172828.jpg', 'dsc2363_20180130172829.jpg', 'dsc2376_20180130172829.jpg', 'dsc2381_20180130172830.jpg', 'dsc2395_20180130172831.jpg', 'dsc2398_20180130172831.jpg', 'dsc2408_20180130172832.jpg', 'dsc2289_20180130172832.jpg', '2018-01-30 19:28:33', '2018-02-01 14:17:30'),
(2, 2, 0, 'correcta', 'Correcta', '.', 'img-6616_20180130175543.jpg', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', 'img-6616_20180130175544.jpg', 'img-6637_20180130175545.jpg', 'img-6670_20180130175545.jpg', 'img-6699_20180130175546.jpg', 'img-6712_20180130175546.jpg', 'img-6725_20180130175547.jpg', 'img-6727_20180130175547.jpg', 'img-6734_20180130175548.jpg', 'img-6751_20180130175549.jpg', 'img-6616_20180130175549.jpg', '2018-01-30 19:55:50', '2018-02-01 14:17:22'),
(3, 2, 0, 'glencore', 'Glencore', '.', 'dsc2885_20180130175818.jpg', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', 'dsc2885_20180130175820.jpg', 'dsc2895_20180130175820.jpg', 'dsc2914_20180130175821.jpg', 'dsc2958_20180130175821.jpg', 'dsc2970_20180130175822.jpg', 'dsc2979_20180130175822.jpg', 'dsc3011_20180130175823.jpg', 'dsc3035_20180130175823.jpg', 'dsc2885_20180130175824.jpg', 'dsc2885_20180130175824.jpg', '2018-01-30 19:58:25', '2018-02-01 14:17:08'),
(4, 2, 0, 'lica-cinelli', 'Lica Cinelli', '.', 'dsc3815_20180130180717.jpg', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', 'dsc3727_20180130180719.jpg', 'dsc3756_20180130180719.jpg', 'dsc3759_20180130180720.jpg', 'dsc3788_20180130180720.jpg', 'dsc3793_20180130180721.jpg', 'dsc3814_20180130180721.jpg', 'dsc3815_20180130180722.jpg', 'dsc3851_20180130180723.jpg', 'dsc3727_20180130180723.jpg', 'dsc3756_20180130180723.jpg', '2018-01-30 20:07:24', '2018-02-01 14:16:57');

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetos_categorias`
--

CREATE TABLE `projetos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `projetos_categorias`
--

INSERT INTO `projetos_categorias` (`id`, `ordem`, `titulo`, `titulo_en`, `slug`, `created_at`, `updated_at`) VALUES
(1, 2, 'VAREJO', '.', 'varejo', '2018-01-24 15:14:43', '2018-02-01 14:20:18'),
(2, 1, 'CORPORATIVO', '.', 'corporativo', '2018-01-24 15:14:46', '2018-02-01 14:20:16');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$UShOjNHdmEYYkhF4C4MpMutUWB2n6qznkvPWWVXNZ7uoZiTzEH90e', 'nbk9wYtADEdWwiDT4fWFY644IdXWwFKr5rQuybLwrmSiJ9tutymlZwmZRJLg', NULL, '2018-01-24 19:31:43');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `depoimentos`
--
ALTER TABLE `depoimentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `destaques`
--
ALTER TABLE `destaques`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destaques_projeto_foreign` (`projeto`);

--
-- Índices de tabela `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `equipe_projetos`
--
ALTER TABLE `equipe_projetos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `equipe_projetos_projetos_id_foreign` (`projetos_id`);

--
-- Índices de tabela `escritorio`
--
ALTER TABLE `escritorio`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `estudos`
--
ALTER TABLE `estudos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `expertise`
--
ALTER TABLE `expertise`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fornecedores_projetos_id_foreign` (`projetos_id`);

--
-- Índices de tabela `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projetos_projetos_categoria_id_foreign` (`projetos_categoria_id`);

--
-- Índices de tabela `projetos_categorias`
--
ALTER TABLE `projetos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `depoimentos`
--
ALTER TABLE `depoimentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `destaques`
--
ALTER TABLE `destaques`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `equipe`
--
ALTER TABLE `equipe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `equipe_projetos`
--
ALTER TABLE `equipe_projetos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `escritorio`
--
ALTER TABLE `escritorio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `estudos`
--
ALTER TABLE `estudos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `expertise`
--
ALTER TABLE `expertise`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `projetos`
--
ALTER TABLE `projetos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `projetos_categorias`
--
ALTER TABLE `projetos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `destaques`
--
ALTER TABLE `destaques`
  ADD CONSTRAINT `destaques_projeto_foreign` FOREIGN KEY (`projeto`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `equipe_projetos`
--
ALTER TABLE `equipe_projetos`
  ADD CONSTRAINT `equipe_projetos_projetos_id_foreign` FOREIGN KEY (`projetos_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `fornecedores`
--
ALTER TABLE `fornecedores`
  ADD CONSTRAINT `fornecedores_projetos_id_foreign` FOREIGN KEY (`projetos_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `projetos`
--
ALTER TABLE `projetos`
  ADD CONSTRAINT `projetos_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `projetos_categorias` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
