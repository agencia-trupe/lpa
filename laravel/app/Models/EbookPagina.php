<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EbookPagina extends Model
{
    protected $table = 'ebook_pagina';

    protected $guarded = ['id'];
    
}
