<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Carbon\Carbon;

class Post extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'blog';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 620,
                'height' => 370,
                'path'   => 'assets/img/blog/'
            ],
            [
                'width'  => 810,
                'height' => 455,
                'path'   => 'assets/img/blog/ampliado/'
            ],
            [
                'width'  => 1200,
                'height' => 350,
                'bw'     => true,
                'path'   => 'assets/img/blog/capa-lg/'
            ],
            [
                'width'  => 585,
                'height' => 280,
                'bw'     => true,
                'path'   => 'assets/img/blog/capa-md/'
            ],
            [
                'width'  => 280,
                'height' => 280,
                'bw'     => true,
                'path'   => 'assets/img/blog/capa/'
            ],
        ]);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}
