<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EbookCadastro extends Model
{
    public $table = 'ebook_cadastro';

    protected $guarded = ['id'];

    protected $fillable = [
        'nome', 'email', 'telefone', 'empresa', 'arquivo_id'
    ];
    
}
