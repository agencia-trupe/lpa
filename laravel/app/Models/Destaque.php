<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Destaque extends Model
{
    protected $table = 'destaques';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function objeto()
    {
        return $this->belongsTo('App\Models\Projeto', 'projeto');
    }

    public function objetoPost()
    {
        return $this->belongsTo('App\Models\Post', 'post');
    }
}
