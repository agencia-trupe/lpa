<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Escritorio extends Model
{
    protected $table = 'escritorio';

    protected $guarded = ['id'];

    public static function upload_banner()
    {
        return CropImage::make('banner', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/escritorio/'
        ]);
    }

}
