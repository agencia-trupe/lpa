<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Projeto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'projetos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('projetos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProjetoCategoria', 'projetos_categoria_id');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 1200,
                'height' => 350,
                'path'   => 'assets/img/projetos/capa-lg/'
            ],
            [
                'width'  => 585,
                'height' => 280,
                'path'   => 'assets/img/projetos/capa-md/'
            ],
            [
                'width'  => 280,
                'height' => 280,
                'path'   => 'assets/img/projetos/capa/'
            ],
        ]);
    }

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 705,
            'height' => 465,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 705,
            'height' => 540,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 465,
            'height' => 310,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 465,
            'height' => 310,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_5()
    {
        return CropImage::make('imagem_5', [
            'width'  => 705,
            'height' => 465,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_6()
    {
        return CropImage::make('imagem_6', [
            'width'  => 1200,
            'height' => 800,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_7()
    {
        return CropImage::make('imagem_7', [
            'width'  => 705,
            'height' => 465,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_8()
    {
        return CropImage::make('imagem_8', [
            'width'  => 465,
            'height' => 700,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_9()
    {
        return CropImage::make('imagem_9', [
            'width'  => 465,
            'height' => 310,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public static function upload_imagem_10()
    {
        return CropImage::make('imagem_10', [
            'width'  => 705,
            'height' => 465,
            'path'   => 'assets/img/projetos/'
        ]);
    }

    public function equipeConsultores()
    {
        return $this->hasMany('App\Models\ProjetoEquipe', 'projetos_id')->ordenados();
    }

    public function fornecedores()
    {
        return $this->hasMany('App\Models\Fornecedor', 'projetos_id')->ordenados();
    }
}
