<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class EbookArquivo extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'ebook_arquivo';

    protected $guarded = ['id'];

    protected $fillable = [
        'titulo', 'prefixo', 'descricao', 'arquivo', 'status'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function cadastros()
    {
        return $this->hasMany('App\Models\EbookCadastro', 'arquivo_id')->ordenados();
    }

}
