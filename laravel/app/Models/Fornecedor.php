<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Fornecedor extends Model
{
    protected $table = 'fornecedores';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProjeto($query, $id)
    {
        return $query->where('projetos_id', $id);
    }
}
