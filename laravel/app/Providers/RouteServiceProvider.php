<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('destaques', 'App\Models\Destaque');
        $router->model('fornecedores', 'App\Models\Fornecedor');
		$router->model('equipe_projetos', 'App\Models\ProjetoEquipe');
		$router->model('projetos', 'App\Models\Projeto');
		$router->model('categorias_projetos', 'App\Models\ProjetoCategoria');
		$router->model('estudos', 'App\Models\Estudo');
		$router->model('escritorio', 'App\Models\Escritorio');
		$router->model('expertise', 'App\Models\Expertise');
		$router->model('clientes', 'App\Models\Cliente');
		$router->model('depoimentos', 'App\Models\Depoimento');
		$router->model('equipe', 'App\Models\Equipe');
		$router->model('blog', 'App\Models\Post');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('ouvidoria', 'App\Models\OuvidoriaRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');
        $router->model('ebook_pagina', 'App\Models\EbookPagina');
        $router->model('ebook_arquivo', 'App\Models\EbookArquivo');
        $router->model('ebook_cadastro', 'App\Models\EbookCadastro');
        $router->model('politica_de_privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('aceite_de_cookies', 'App\Models\AceiteDeCookies');

        $router->bind('projetos_categoria', function($value) {
            return \App\Models\ProjetoCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('projetos_slug', function($value) {
            return \App\Models\Projeto::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('post_slug', function($value) {
            return \App\Models\Post::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('estudos_slug', function($value) {
            return \App\Models\Estudo::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
