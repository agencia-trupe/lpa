<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('ouvidoriaNaoLidos', \App\Models\OuvidoriaRecebido::naoLidos()->count());
        });

        view()->composer('frontend.common.*', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('emails.ebook', function($view) {
            $view->with('url_ebook', \App\Models\EbookCadastro::first());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
