<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Post;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('data', 'DESC')->orderBy('id', 'DESC')->paginate(7);

        return view('frontend.blog', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('frontend.blog-show', compact('post'));
    }
}
