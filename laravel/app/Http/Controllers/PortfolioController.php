<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;
use App\Models\ProjetoCategoria;
use App\Models\Estudo;

class PortfolioController extends Controller
{
    public function index(ProjetoCategoria $categoria)
    {
        $categorias = ProjetoCategoria::ordenados()->get();

        if (!$categoria->exists) {
            $projetos = Projeto::leftJoin('projetos_categorias as cat', 'cat.id', '=', 'projetos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('projetos.*')
                ->ordenados()->get();
        } else {
            $projetos = $categoria->projetos;
        }

        return view('frontend.projetos.index', compact('categorias', 'categoria', 'projetos'));
    }

    public function show(ProjetoCategoria $categoria, Projeto $projeto)
    {
        $categorias = ProjetoCategoria::ordenados()->get();

        return view('frontend.projetos.show', compact('categorias', 'categoria', 'projeto'));
    }

    public function estudos()
    {
        $estudos = Estudo::ordenados()->get();

        return view('frontend.estudos.index', compact('estudos'));
    }

    public function estudosShow(Estudo $estudo)
    {
        return view('frontend.estudos.show', compact('estudo'));
    }
}
