<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Equipe;

class EquipeController extends Controller
{
    public function index()
    {
        $equipe = Equipe::ordenados()->get();

        return view('frontend.equipe', compact('equipe'));
    }
}
