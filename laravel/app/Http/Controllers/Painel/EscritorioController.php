<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EscritorioRequest;
use App\Http\Controllers\Controller;

use App\Models\Escritorio;

class EscritorioController extends Controller
{
    public function index()
    {
        $registro = Escritorio::first();

        return view('painel.escritorio.edit', compact('registro'));
    }

    public function update(EscritorioRequest $request, Escritorio $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['banner'])) $input['banner'] = Escritorio::upload_banner();

            $registro->update($input);

            return redirect()->route('painel.escritorio.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
