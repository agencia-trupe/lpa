<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoCategoria;

class ProjetosController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = ProjetoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (ProjetoCategoria::find($filtro)) {
            $registros = Projeto::where('projetos_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = Projeto::leftJoin('projetos_categorias as cat', 'cat.id', '=', 'projetos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('projetos.*')
                ->ordenados()->get();
        }

        return view('painel.projetos.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.projetos.create', compact('categorias'));
    }

    public function store(ProjetosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Projeto::upload_capa();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Projeto::upload_imagem_1();

            if (isset($input['imagem_2'])) $input['imagem_2'] = Projeto::upload_imagem_2();

            if (isset($input['imagem_3'])) $input['imagem_3'] = Projeto::upload_imagem_3();

            if (isset($input['imagem_4'])) $input['imagem_4'] = Projeto::upload_imagem_4();

            if (isset($input['imagem_5'])) $input['imagem_5'] = Projeto::upload_imagem_5();

            if (isset($input['imagem_6'])) $input['imagem_6'] = Projeto::upload_imagem_6();

            if (isset($input['imagem_7'])) $input['imagem_7'] = Projeto::upload_imagem_7();

            if (isset($input['imagem_8'])) $input['imagem_8'] = Projeto::upload_imagem_8();

            if (isset($input['imagem_9'])) $input['imagem_9'] = Projeto::upload_imagem_9();

            if (isset($input['imagem_10'])) $input['imagem_10'] = Projeto::upload_imagem_10();

            Projeto::create($input);

            return redirect()->route('painel.projetos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Projeto $registro)
    {
        $categorias = $this->categorias;

        return view('painel.projetos.edit', compact('registro', 'categorias'));
    }

    public function update(ProjetosRequest $request, Projeto $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Projeto::upload_capa();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Projeto::upload_imagem_1();

            if (isset($input['imagem_2'])) $input['imagem_2'] = Projeto::upload_imagem_2();

            if (isset($input['imagem_3'])) $input['imagem_3'] = Projeto::upload_imagem_3();

            if (isset($input['imagem_4'])) $input['imagem_4'] = Projeto::upload_imagem_4();

            if (isset($input['imagem_5'])) $input['imagem_5'] = Projeto::upload_imagem_5();

            if (isset($input['imagem_6'])) $input['imagem_6'] = Projeto::upload_imagem_6();

            if (isset($input['imagem_7'])) $input['imagem_7'] = Projeto::upload_imagem_7();

            if (isset($input['imagem_8'])) $input['imagem_8'] = Projeto::upload_imagem_8();

            if (isset($input['imagem_9'])) $input['imagem_9'] = Projeto::upload_imagem_9();

            if (isset($input['imagem_10'])) $input['imagem_10'] = Projeto::upload_imagem_10();

            $registro->update($input);

            return redirect()->route('painel.projetos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.projetos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function removerImagem(Projeto $registro, $id)
    {
        if ($id < 2 || $id > 10) {
            return redirect()->route('painel.projetos.edit', $registro->id)->withErrors(['Erro ao excluir registro: Imagem não existe.']);
        }

        try {

            $registro->update([
                'imagem_'.$id => '',
            ]);

            return redirect()->route('painel.projetos.edit', $registro->id)->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

}
