<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DestaquesRequest;
use App\Http\Controllers\Controller;

use App\Models\Destaque;
use App\Models\Projeto;
use App\Models\Post;

class DestaquesController extends Controller
{
    public function index()
    {
        $registros = Destaque::ordenados()->get();

        return view('painel.destaques.index', compact('registros'));
    }

    public function create()
    {
        $projetosCadastrados = array_filter(Destaque::lists('projeto')->toArray());
        $postsCadastrados    = array_filter(Destaque::lists('post')->toArray());

        $projetos = Projeto::leftJoin('projetos_categorias as cat', 'cat.id', '=', 'projetos_categoria_id')
            ->orderBy('cat.ordem', 'ASC')
            ->orderBy('cat.id', 'DESC')
            ->select('projetos.*')
            ->whereNotIn('projetos.id', $projetosCadastrados)
            ->ordenados()->lists('titulo', 'id');

        $posts = Post::orderBy('data', 'DESC')
            ->orderBy('id', 'DESC')
            ->whereNotIn('id', $postsCadastrados)
            ->lists('titulo', 'id');

        return view('painel.destaques.create', compact('projetos', 'posts'));
    }

    public function store(DestaquesRequest $request)
    {
        try {

            if ($request->projeto && $request->post) {
                throw new \Exception('Selecione apenas um campo.');
            }

            $input = $request->all();

            if (!$request->projeto) $input['projeto'] = null;
            if (!$request->post) $input['post'] = null;

            Destaque::create($input);

            return redirect()->route('painel.destaques.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Destaque $registro)
    {
        $projetosCadastrados = array_filter(Destaque::where('id', '!=', $registro->id)->lists('projeto')->toArray());
        $postsCadastrados    = array_filter(Destaque::where('id', '!=', $registro->id)->lists('post')->toArray());

        $projetos = Projeto::leftJoin('projetos_categorias as cat', 'cat.id', '=', 'projetos_categoria_id')
            ->orderBy('cat.ordem', 'ASC')
            ->orderBy('cat.id', 'DESC')
            ->select('projetos.*')
            ->whereNotIn('projetos.id', $projetosCadastrados)
            ->ordenados()->lists('titulo', 'id');

        $posts = Post::orderBy('data', 'DESC')
            ->orderBy('id', 'DESC')
            ->whereNotIn('id', $postsCadastrados)
            ->lists('titulo', 'id');

        return view('painel.destaques.edit', compact('registro', 'projetos', 'posts'));
    }

    public function update(DestaquesRequest $request, Destaque $registro)
    {
        try {

            if ($request->projeto && $request->post) {
                throw new \Exception('Selecione apenas um campo.');
            }

            $input = $request->all();

            if (!$request->projeto) $input['projeto'] = null;
            if (!$request->post) $input['post'] = null;

            $registro->update($input);

            return redirect()->route('painel.destaques.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Destaque $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.destaques.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
