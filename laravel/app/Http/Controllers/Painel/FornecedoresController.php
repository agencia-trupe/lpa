<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FornecedoresRequest;
use App\Http\Controllers\Controller;

use App\Models\Fornecedor;
use App\Models\Projeto;

class FornecedoresController extends Controller
{
    public function index(Projeto $projeto)
    {
        $registros = Fornecedor::projeto($projeto->id)->ordenados()->get();

        return view('painel.fornecedores.index', compact('registros', 'projeto'));
    }

    public function create(Projeto $projeto)
    {
        return view('painel.fornecedores.create', compact('projeto'));
    }

    public function store(FornecedoresRequest $request, Projeto $projeto)
    {
        try {

            $input = $request->all();

            Fornecedor::create($input);

            return redirect()->route('painel.projetos.fornecedores.index', $projeto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Projeto $projeto, Fornecedor $registro)
    {
        return view('painel.fornecedores.edit', compact('registro', 'projeto'));
    }

    public function update(FornecedoresRequest $request, Projeto $projeto, Fornecedor $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.projetos.fornecedores.index', $projeto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $projeto, Fornecedor $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.projetos.fornecedores.index', $projeto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
