<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ExpertiseRequest;
use App\Http\Controllers\Controller;

use App\Models\Expertise;

class ExpertiseController extends Controller
{
    public function index()
    {
        $registros = Expertise::ordenados()->get();

        return view('painel.expertise.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.expertise.create');
    }

    public function store(ExpertiseRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Expertise::upload_imagem();

            Expertise::create($input);

            return redirect()->route('painel.expertise.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Expertise $registro)
    {
        return view('painel.expertise.edit', compact('registro'));
    }

    public function update(ExpertiseRequest $request, Expertise $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Expertise::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.expertise.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Expertise $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.expertise.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
