<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosEquipeRequest;
use App\Http\Controllers\Controller;

use App\Models\ProjetoEquipe;
use App\Models\Projeto;

class ProjetosEquipeController extends Controller
{
    public function index(Projeto $projeto)
    {
        $registros = ProjetoEquipe::projeto($projeto->id)->ordenados()->get();

        return view('painel.equipe_projetos.index', compact('registros', 'projeto'));
    }

    public function create(Projeto $projeto)
    {
        return view('painel.equipe_projetos.create', compact('projeto'));
    }

    public function store(ProjetosEquipeRequest $request, Projeto $projeto)
    {
        try {

            $input = $request->all();

            ProjetoEquipe::create($input);

            return redirect()->route('painel.projetos.equipe.index', $projeto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Projeto $projeto, ProjetoEquipe $registro)
    {
        return view('painel.equipe_projetos.edit', compact('registro', 'projeto'));
    }

    public function update(ProjetosEquipeRequest $request, Projeto $projeto, ProjetoEquipe $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.projetos.equipe.index', $projeto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $projeto, ProjetoEquipe $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.projetos.equipe.index', $projeto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
