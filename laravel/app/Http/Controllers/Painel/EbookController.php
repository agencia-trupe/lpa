<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\EbookArquivoRequest;
use App\Models\EbookArquivo;
use App\Models\EbookCadastro;
use App\Models\EbookPagina;
use App\Http\Requests\EbookPaginaRequest;
use Maatwebsite\Excel\Facades\Excel;

class EbookController extends Controller
{

    private $arquivos;

    public function __construct()
    {
        $this->arquivos = EbookArquivo::ordenados()->lists('titulo', 'id');
    }

    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'N' => 'Ñ'
        );

        return $conversao;
    }

    public function index()
    {
        $arquivos = EbookArquivo::ordenados()->get();
        $status = [false => 'Inativo', true => 'Ativo'];

        return view('painel.ebook.arquivos.index', compact('arquivos', 'status'));
    }

    public function create()
    {
        $status = [false => 'Inativo', true => 'Ativo'];

        return view('painel.ebook.arquivos.create', compact('status'));
    }

    public function store(EbookArquivoRequest $request)
    {
        try {

            $input = $request->all();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '\assets\ebook';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }
            EbookArquivo::create($input);

            return redirect()->route('ebook.arquivos.index')->with('success', 'Arquivo adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar arquivo: ' . $e->getMessage()]);
        }
    }

    public function edit(EbookArquivo $arquivo)
    {
        $status = [false => 'Inativo', true => 'Ativo'];

        return view('painel.ebook.arquivos.edit', compact('arquivo', 'status'));
    }

    public function update(Request $request, EbookArquivo $arquivo)
    {
        try {

            $input = $request->all();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '\assets\ebook';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $arquivo->update($input);

            return redirect()->route('ebook.arquivos.index')->with('success', 'Arquivo alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar arquivo: ' . $e->getMessage()]);
        }
    }

    public function destroy(EbookArquivo $arquivo)
    {
        try {

            $arquivo->delete();

            return redirect()->route('ebook.arquivos.index')->with('success', 'Arquivo excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir arquivo: ' . $e->getMessage()]);
        }
    }

    public function indexPagina()
    {
        $registro = EbookPagina::first();

        return view('painel.ebook.pagina.edit', compact('registro'));
    }

    public function updatePagina(EbookPaginaRequest $request, EbookPagina $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('ebook.pagina.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function downloads(EbookCadastro $cadastros)
    {
        $cadastros = EbookCadastro::orderBy('created_at', 'DESC')->get();

        $arquivos = EbookArquivo::all();

        return view('painel.ebook.downloads.index', compact('cadastros', 'arquivos'));
    }

    public function export()
    {
        $row[] = array('Data', 'Nome', 'E-mail', 'Telefone', 'Empresa', 'Arquivo');

        $exportDados = EbookCadastro::all();

        foreach ($exportDados as $dados) {

            $arquivos = EbookArquivo::where('id', $dados->arquivo_id)->get();

            foreach ($arquivos as $arquivo) {

                $row[] = array(
                    $dados->created_at->format('d/m/Y h:i'),
                    $dados->nome,
                    $dados->email,
                    $dados->telefone,
                    $dados->empresa,
                    $arquivo->titulo
                );
            }
        }

        return Excel::create('Cadastros para Downloads', function ($excel) use ($row) {
            $excel->setTitle('Cadastros');
            $excel->sheet('Cadastros', function ($sheet) use ($row) {
                $sheet->fromArray($row, null, 'A1', false, false);
            });
        })->export('csv');
    }
}
