<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EstudosRequest;
use App\Http\Controllers\Controller;

use App\Models\Estudo;

class EstudosController extends Controller
{
    public function index()
    {
        $registros = Estudo::ordenados()->get();

        return view('painel.estudos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.estudos.create');
    }

    public function store(EstudosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Estudo::upload_capa();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Estudo::upload_imagem_1();

            if (isset($input['imagem_2'])) $input['imagem_2'] = Estudo::upload_imagem_2();

            if (isset($input['imagem_3'])) $input['imagem_3'] = Estudo::upload_imagem_3();

            if (isset($input['imagem_4'])) $input['imagem_4'] = Estudo::upload_imagem_4();

            if (isset($input['imagem_5'])) $input['imagem_5'] = Estudo::upload_imagem_5();

            if (isset($input['imagem_6'])) $input['imagem_6'] = Estudo::upload_imagem_6();

            if (isset($input['imagem_7'])) $input['imagem_7'] = Estudo::upload_imagem_7();

            if (isset($input['imagem_8'])) $input['imagem_8'] = Estudo::upload_imagem_8();

            if (isset($input['imagem_9'])) $input['imagem_9'] = Estudo::upload_imagem_9();

            if (isset($input['imagem_10'])) $input['imagem_10'] = Estudo::upload_imagem_10();

            Estudo::create($input);

            return redirect()->route('painel.estudos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Estudo $registro)
    {
        return view('painel.estudos.edit', compact('registro'));
    }

    public function update(EstudosRequest $request, Estudo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Estudo::upload_capa();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Estudo::upload_imagem_1();

            if (isset($input['imagem_2'])) $input['imagem_2'] = Estudo::upload_imagem_2();

            if (isset($input['imagem_3'])) $input['imagem_3'] = Estudo::upload_imagem_3();

            if (isset($input['imagem_4'])) $input['imagem_4'] = Estudo::upload_imagem_4();

            if (isset($input['imagem_5'])) $input['imagem_5'] = Estudo::upload_imagem_5();

            if (isset($input['imagem_6'])) $input['imagem_6'] = Estudo::upload_imagem_6();

            if (isset($input['imagem_7'])) $input['imagem_7'] = Estudo::upload_imagem_7();

            if (isset($input['imagem_8'])) $input['imagem_8'] = Estudo::upload_imagem_8();

            if (isset($input['imagem_9'])) $input['imagem_9'] = Estudo::upload_imagem_9();

            if (isset($input['imagem_10'])) $input['imagem_10'] = Estudo::upload_imagem_10();

            $registro->update($input);

            return redirect()->route('painel.estudos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Estudo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.estudos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function removerImagem(Estudo $registro, $id)
    {
        if ($id < 2 || $id > 10) {
            return redirect()->route('painel.estudos.edit', $registro->id)->withErrors(['Erro ao excluir registro: Imagem não existe.']);
        }

        try {

            $registro->update([
                'imagem_'.$id => '',
            ]);

            return redirect()->route('painel.estudos.edit', $registro->id)->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

}
