<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BlogRequest;
use App\Http\Controllers\Controller;

use App\Models\Post;

class BlogController extends Controller
{
    public function index()
    {
        $registros = Post::orderBy('data', 'DESC')->orderBy('id', 'DESC')->paginate(10);

        return view('painel.blog.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.blog.create');
    }

    public function store(BlogRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Post::upload_capa();

            Post::create($input);

            return redirect()->route('painel.blog.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Post $registro)
    {
        return view('painel.blog.edit', compact('registro'));
    }

    public function update(BlogRequest $request, Post $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Post::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.blog.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Post $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.blog.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
