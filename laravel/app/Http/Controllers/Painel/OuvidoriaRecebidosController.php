<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OuvidoriaRecebido;

class OuvidoriaRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = OuvidoriaRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.contato.ouvidoria.index', compact('contatosrecebidos'));
    }

    public function show(OuvidoriaRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.ouvidoria.show', compact('contato'));
    }

    public function destroy(OuvidoriaRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.ouvidoria.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(OuvidoriaRecebido $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.contato.ouvidoria.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
