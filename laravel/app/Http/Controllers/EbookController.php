<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\EbookPagina;
use App\Models\EbookArquivo;
use App\Models\EbookCadastro;
use App\Http\Requests\EbookCadastroRequest;

use Illuminate\Support\Facades\Mail;

class EbookController extends Controller
{
    private $dadosPagina;
    private $dadosArquivo;

    public function __construct(EbookPagina $dadosPagina, EbookArquivo $dadosArquivo)
    {
        $this->dadosArquivo = $dadosArquivo;
        $this->dadosPagina = $dadosPagina;
    }

    public function index()
    {
        $dadosPagina = $this->dadosPagina->first();
        $dadosArquivo = $this->dadosArquivo->ordenados()->where('status', 1)->get();

        return view('frontend.ebook.index', compact('dadosPagina', 'dadosArquivo'));
    }

    public function show($slug)
    {
        $dadosPagina = $this->dadosPagina->first();
        $dadosArquivo = $this->dadosArquivo->ordenados()->where('status', 1)->get();

        $arquivoSlug = $this->dadosArquivo->ordenados()->where('slug', $slug)->first();

        return view('frontend.ebook.create', compact('dadosPagina', 'dadosArquivo', 'arquivoSlug'));
    }

    public function store(EbookCadastroRequest $request)
    {
        $dadosPagina = $this->dadosPagina->first();
        $dadosArquivo = $this->dadosArquivo->ordenados()->where('status', 1)->get();

        $input = $request->all();
        $ebookCadastro = EbookCadastro::create($input);

        $this->sendMail($request);

        return view('frontend.ebook.mensagem', compact('dadosPagina', 'dadosArquivo'));
    }

    private function createUrl(Request $request)
    {
        $emailBase64 = base64_encode($request->email);

        $ebookArquivo = $this->dadosArquivo->find($request->arquivo_id);

        $urlEbook = "http://www.lpa.arq.br/ebook/download/" . $ebookArquivo->slug . "/" . $emailBase64;

        return $urlEbook;
    }

    private function sendMail(Request $request)
    {
        $dadosEmail = array(
            'nome' => $request['nome'],
            'ebook' => $this->dadosArquivo->find($request->arquivo_id),
            'link' => $this->createUrl($request)
        );

        if (isset($request['email'])) {
            Mail::send('emails.ebook', $dadosEmail, function ($message) use ($request) {
                $message->to($request->email, $request->nome)
                    ->subject('[EBOOK] LPA Arquitetura')
                    ->from(config('mail.username'), '[EBOOK] LPA Arquitetura');
            });
        }
    }

    public function download($slug, $email)
    {
        $emailBase64 = base64_decode($email);
        
        $emailCadastro = EbookCadastro::where('email', $emailBase64)->first();
        if (!$emailCadastro) return redirect()->back();

        $ebookArquivo = $this->dadosArquivo->where('slug', $slug)->first();
        if (!$ebookArquivo) return redirect()->back();

        $ebookPath = public_path() . "/assets/ebook/" . $ebookArquivo->arquivo;

        return response()->download($ebookPath);
    }
}
