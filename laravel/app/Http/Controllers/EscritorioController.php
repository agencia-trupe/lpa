<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Escritorio;
use App\Models\Expertise;
use App\Models\Cliente;
use App\Models\Depoimento;

class EscritorioController extends Controller
{
    public function index()
    {
        $escritorio = Escritorio::first();

        return view('frontend.escritorio.escritorio', compact('escritorio'));
    }

    public function expertise()
    {
        $expertise = Expertise::ordenados()->get();

        return view('frontend.escritorio.expertise', compact('expertise'));
    }

    public function clientes()
    {
        $clientes = Cliente::ordenados()->get();
        $depoimentos = Depoimento::ordenados()->get();

        return view('frontend.escritorio.clientes', compact('clientes', 'depoimentos'));
    }
}
