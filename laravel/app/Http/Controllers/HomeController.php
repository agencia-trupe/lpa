<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AceiteDeCookies;
use App\Models\Destaque;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $destaques = Destaque::ordenados()->get();
        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();

        return view('frontend.home', compact('destaques', 'verificacao'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
