<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Contato;
use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Requests\OuvidoriaRecebidosRequest;
use App\Models\ContatoRecebido;
use App\Models\OuvidoriaRecebido;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $input, function ($message) use ($request, $contato) {
                $message->to($contato->email, 'LPA')
                        ->subject('[CONTATO] LPA')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }

    public function postOuvidoria(OuvidoriaRecebidosRequest $request, OuvidoriaRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email) && $input['email']) {
            \Mail::send('emails.ouvidoria', $input, function ($message) use ($request, $contato) {
                $message->to($contato->email, 'LPA')
                        ->subject('[OUVIDORIA] LPA')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }
}
