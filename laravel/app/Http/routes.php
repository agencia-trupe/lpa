<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('o-escritorio', 'EscritorioController@index')->name('o-escritorio');
    Route::get('o-escritorio/expertise', 'EscritorioController@expertise')->name('o-escritorio.expertise');
    Route::get('o-escritorio/clientes', 'EscritorioController@clientes')->name('o-escritorio.clientes');
    Route::get('projetos/{projetos_categoria?}', 'PortfolioController@index')->name('projetos');
    Route::get('projetos/{projetos_categoria}/{projetos_slug}', 'PortfolioController@show')->name('projetos.show');
    //Route::get('portfolio/estudos', 'PortfolioController@estudos')->name('portfolio.estudos');
    //Route::get('portfolio/estudos/{estudos_slug}', 'PortfolioController@estudosShow')->name('portfolio.estudos.show');
    Route::get('equipe', 'EquipeController@index')->name('equipe');
    Route::get('blog', 'BlogController@index')->name('blog');
    Route::get('blog/{post_slug}', 'BlogController@show')->name('blog.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('ouvidoria', 'ContatoController@postOuvidoria')->name('contato.ouvidoria');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en') Session::put('locale', $idioma);
        return redirect()->back();
    })->name('lang');

    // EBOOK
    Route::resource('ebook', 'EbookController', ['only' => ['index', 'show', 'store']]);
    Route::get('ebook/download/{slug}/{email}', 'EbookController@download')->name('ebook.download');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('destaques', 'DestaquesController');
        Route::resource('projetos/categorias', 'ProjetosCategoriasController', ['parameters' => ['categorias' => 'categorias_projetos']]);
        Route::resource('projetos', 'ProjetosController');
        Route::get('projetos/{projetos}/remover-imagem/{id}', 'ProjetosController@removerImagem')->name('projetos.remover-imagem');
        //Route::resource('projetos.fornecedores', 'FornecedoresController');
		//Route::resource('projetos.equipe', 'ProjetosEquipeController', ['parameters' => ['equipe' => 'equipe_projetos']]);
		//Route::resource('estudos', 'EstudosController');
        //Route::get('estudos/{estudos}/remover-imagem/{id}', 'EstudosController@removerImagem')->name('estudos.remover-imagem');
		Route::resource('escritorio', 'EscritorioController', ['only' => ['index', 'update']]);
		Route::resource('expertise', 'ExpertiseController');
		Route::resource('clientes', 'ClientesController');
		Route::resource('depoimentos', 'DepoimentosController');
		Route::resource('equipe', 'EquipeController');
		Route::resource('blog', 'BlogController');
        Route::get('blog/{blog}/remover-imagem/{id}', 'BlogController@removerImagem')->name('blog.remover-imagem');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::get('contato/ouvidoria/recebidos/{ouvidoria}/toggle', ['as' => 'painel.contato.ouvidoria.toggle', 'uses' => 'OuvidoriaRecebidosController@toggle']);
        Route::resource('contato/ouvidoria', 'OuvidoriaRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');

        // EBOOK
        Route::get('ebook/arquivos', 'EbookController@index')->name('ebook.arquivos.index');
        Route::get('ebook/arquivos/create', 'EbookController@create')->name('ebook.arquivos.create');
        Route::post('ebook/arquivos/create', 'EbookController@store')->name('ebook.arquivos.store');
        Route::get('ebook/arquivos/edit/{arquivo}', 'EbookController@edit')->name('ebook.arquivos.edit');
        Route::post('ebook/arquivos/edit/{arquivo}', 'EbookController@update')->name('ebook.arquivos.update');
        Route::delete('ebook/arquivos/delete/{arquivo}', 'EbookController@destroy')->name('ebook.arquivos.destroy');
        Route::get('ebook/pagina', 'EbookController@indexPagina')->name('ebook.pagina.index');
        Route::post('ebook/pagina/{registro}', 'EbookController@updatePagina')->name('ebook.pagina.update');
        Route::get('ebook/downloads', 'EbookController@downloads')->name('ebook.downloads');
        Route::get('ebook/downloads/export', 'EbookController@export')->name('ebook.downloads.export');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
