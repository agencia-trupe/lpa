<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DestaquesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'projeto' => 'required_without:post',
            'post'    => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required_without' => 'Preencha um dos campos.'
        ];
    }
}
