<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjetosEquipeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'projetos_id' => 'required',
            'categoria' => 'required',
            'categoria_en' => 'required',
            'equipe_en' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
