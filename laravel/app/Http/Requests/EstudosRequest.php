<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EstudosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'titulo_en' => 'required',
            'capa' => 'required|image',
            'cidade' => '',
            'area' => '',
            'ano' => 'required',
            'equipe' => '',
            'descricao' => '',
            'diferenciais_tecnicos' => '',
            'padroes_de_cores' => '',
            'novos_conceitos' => '',
            'imagem_1' => 'required|image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
            'imagem_5' => 'image',
            'imagem_6' => 'image',
            'imagem_7' => 'image',
            'imagem_8' => 'image',
            'imagem_9' => 'image',
            'imagem_10' => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['imagem_1'] = 'image';
            $rules['imagem_2'] = 'image';
            $rules['imagem_3'] = 'image';
            $rules['imagem_4'] = 'image';
            $rules['imagem_5'] = 'image';
            $rules['imagem_6'] = 'image';
            $rules['imagem_7'] = 'image';
            $rules['imagem_8'] = 'image';
            $rules['imagem_9'] = 'image';
            $rules['imagem_10'] = 'image';
        }

        return $rules;
    }
}
