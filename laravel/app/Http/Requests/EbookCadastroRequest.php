<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EbookCadastroRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required',
            'empresa'  => 'required',
        ];
    }

    public function messages() {
        return [
            'required' => trans('frontend.contato.erro'),
            'email'    => trans('frontend.contato.erro'),
        ];
    }
}
