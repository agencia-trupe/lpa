<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

class EbookArquivoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo'     => 'required',
            'prefixo'    => 'required',
            'descricao'  => 'required',
            'arquivo'    => 'required',
        ];
    }

    public function messages() {
        return [
            'required' => trans('frontend.contato.erro'),
        ];
    }
}
