<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EquipeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'imagem' => 'required|image',
            'cargo' => 'required',
            'cargo_en' => 'required',
            'e_mail' => 'required|email',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
