<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepoimentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'titulo_en' => 'required',
            'subtitulo' => 'required',
            'subtitulo_en' => 'required',
            'texto' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
