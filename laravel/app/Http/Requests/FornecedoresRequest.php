<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FornecedoresRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'material' => 'required',
            'material_en' => 'required',
            'fornecedor' => 'required',
            'fornecedor_en' => 'required',
            'detalhes' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
