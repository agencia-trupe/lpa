<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EscritorioRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'banner' => 'image',
            'quem_somos' => 'required',
            'quem_somos_en' => 'required',
            'o_escritorio' => 'required',
            'o_escritorio_en' => 'required',
            'missao' => 'required',
            'missao_en' => 'required',
            'visao_en' => 'required',
            'premios' => 'required',
            'premios_en' => 'required',
        ];
    }
}
