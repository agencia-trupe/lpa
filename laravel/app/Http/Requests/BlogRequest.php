<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data' => 'required',
            'titulo' => 'required',
            'titulo_en' => 'required',
            'texto' => 'required',
            'texto_en' => 'required',
            'imagem_1' => 'image',
            'imagem_1_legenda' => '',
            'imagem_2' => 'image',
            'imagem_2_legenda' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_1'] = 'image';
            $rules['imagem_2'] = 'image';
        }

        return $rules;
    }
}
