@extends('frontend.common.template')

@section('content')

    <div class="blog">
        <div class="center">
            <div class="blog-show">
                <img src="{{ asset('assets/img/blog/ampliado/'.$post->capa) }}" alt="">
                <h1>{{ Tools::traducao($post, 'titulo') }}</h1>
                <div class="texto">
                    {!! Tools::traducao($post, 'texto') !!}
                </div>
                <div class="facebook">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.12&appId=204616743456849&autoLogAppEvents=1';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-like" data-href="{{ request()->url() }}" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                    <div class="fb-comments" style="display:block; margin-top: 20px" data-href="{{ request()->url() }}" data-numposts="10" data-width="100%"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
