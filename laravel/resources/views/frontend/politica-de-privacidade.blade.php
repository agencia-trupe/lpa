@extends('frontend.common.template')

@section('content')

<div class="politica-de-privacidade">
    <div class="center">
        <h1 class="titulo-politica">{{ trans('frontend.footer.politica') }}</h1>
        <div class="texto-politica">{!! Tools::traducao($politica, 'texto') !!}</div>
    </div>
</div>

@endsection