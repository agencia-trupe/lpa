@extends('frontend.common.template')

@section('content')

    <div class="blog">
        <div class="center">
            @foreach($posts as $post)
            <a href="{{ route('blog.show', $post->slug) }}" class="blog-post">
                <div class="texto">
                    <h3>{{ Tools::traducao($post, 'titulo') }}</h3>
                    <p>{!! Tools::traducao($post, 'chamada') !!}</p>
                </div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/blog/'.$post->capa) }}" alt="">
                </div>
            </a>
            @endforeach

            {!! $posts->render() !!}
        </div>
    </div>

@endsection
