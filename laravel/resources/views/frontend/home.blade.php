@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="center">
        @if(count($destaques))
            <?php $primeiro = $destaques->shift(); ?>
            @if($primeiro->projeto)
            <a href="{{ route('projetos.show', [$primeiro->objeto->categoria->slug, $primeiro->objeto->slug]) }}" class="thumb-full">
                <div class="imagem">
                    <img src="{{ asset('assets/img/projetos/capa-lg/'.$primeiro->objeto->capa) }}" alt="" class="desktop">
                </div>
                <img src="{{ asset('assets/img/projetos/capa-md/'.$primeiro->objeto->capa) }}" alt="" class="mobile">
                <p>
                    <span>{{ Tools::traducao($primeiro->objeto->categoria, 'titulo') }}</span>
                    {{ Tools::traducao($primeiro->objeto, 'titulo') }}
                </p>
            </a>
            @else
            <a href="{{ route('blog.show', $primeiro->objetoPost->slug) }}" class="thumb-full">
                <div class="imagem">
                    <img src="{{ asset('assets/img/blog/capa-lg/'.$primeiro->objetoPost->capa) }}" alt="" class="desktop">
                </div>
                <img src="{{ asset('assets/img/blog/capa-md/'.$primeiro->objetoPost->capa) }}" alt="" class="mobile">
                <p>
                    <span>BLOG</span>
                    {{ Tools::traducao($primeiro->objetoPost, 'titulo') }}
                </p>
            </a>
            @endif

            @foreach($destaques->chunk(10) as $chunk)
                <?php $i = 0; ?>
                <div>
                @foreach($chunk as $destaque)
                <?php
                    $i++;

                    if (in_array($i, [1, 2, 3, 4, 6, 7, 8, 9])) {
                        $classe = 'thumb-1';
                        $pasta  = 'capa';
                    } else {
                        $classe = 'thumb-2';
                        $pasta  = 'capa-md';
                    }
                ?>
                    @if($destaque->projeto)
                    <a href="{{ route('projetos.show', [$destaque->objeto->categoria->slug, $destaque->objeto->slug]) }}" class="{{ $classe }}">
                        <div class="imagem">
                            <img src="{{ asset('assets/img/projetos/'.$pasta.'/'.$destaque->objeto->capa) }}" alt="">
                        </div>
                        <p>
                            <span>{{ Tools::traducao($destaque->objeto->categoria, 'titulo') }}</span>
                            {{ Tools::traducao($destaque->objeto, 'titulo') }}
                        </p>
                    </a>
                    @else
                    <a href="{{ route('blog.show', $destaque->objetoPost->slug) }}" class="{{ $classe }}">
                        <div class="imagem">
                            <img src="{{ asset('assets/img/blog/'.$pasta.'/'.$destaque->objetoPost->capa) }}" alt="">
                        </div>
                        <p>
                            <span>BLOG</span>
                            {{ Tools::traducao($destaque->objetoPost, 'titulo') }}
                        </p>
                    </a>
                    @endif
                @endforeach
                </div>
            @endforeach
        @endif
        </div>
    </div>

@endsection
