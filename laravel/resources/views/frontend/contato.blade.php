@extends('frontend.common.template')

@section('content')

<div class="contato-page">
    <div class="center">
        <div class="informacoes">
            <!-- <h3>{{ trans('frontend.nav.contato') }}</h3>
                <p>{{ $contato->telefone }}</p>
                <p>{!! Tools::traducao($contato, 'endereco') !!}</p>
                <p>
                @foreach(['facebook', 'instagram', 'linkedin', 'youtube', 'pinterest'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" target="_blank">
                        <img src="{{ asset('assets/img/layout/'.$s.'-icone.png') }}" alt="">
                    </a>
                    @endif
                @endforeach
                </p>
                <h3>{{ trans('frontend.contato.contate') }}</h3>
                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                    <div class="botao">
                        <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                    </div>
                    <div id="form-contato-response"></div>
                </form> -->
            <div role="main" id="contato-78089f730a917df9cc89"></div>

            <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>

            <script type="text/javascript">
                new RDStationForms('contato-78089f730a917df9cc89', 'UA-178635387-1').createForm();
            </script>

        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>
</div>

<div class="ouvidoria">
    <div class="center">
        <div class="informacoes">
            <h3>{{ trans('frontend.contato.ouvidoria') }}</h3>
            <p>{!! Tools::traducao($contato, 'ouvidoria') !!}</p>
        </div>

        <form action="" id="form-ouvidoria" method="POST">
            <input type="text" name="nome" id="nome_o" placeholder="{{ trans('frontend.contato.nome') }} {{ trans('frontend.contato.opcional') }}">
            <input type="email" name="email" id="email_o" placeholder="e-mail {{ trans('frontend.contato.opcional') }}">
            <textarea name="mensagem" id="mensagem_o" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
            <div class="botao">
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
            </div>
            <div id="form-ouvidoria-response"></div>
        </form>
    </div>
</div>

@endsection