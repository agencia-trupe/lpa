@extends('frontend.common.template')

@section('content')

    <div class="equipe">
        <div class="center">
            <h2>{{ trans('frontend.nav.equipe') }}</h2>

            <div class="equipe-thumbs">
                @foreach($equipe as $membro)
                <a href="mailto:{{ $membro->e_mail }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/equipe/'.$membro->imagem) }}" alt="">
                        <span class="nome">{{ $membro->e_mail }}</span>
                    </div>
                    <div class="dados">
                        <h5>{{ $membro->nome }}</h5>
                        <p>{{ Tools::traducao($membro, 'cargo') }}</p>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
