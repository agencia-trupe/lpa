@extends('frontend.common.template')

@section('content')

    <div class="escritorio">
        <div class="banner" style="background-image:url('{{ asset('assets/img/escritorio/'.$escritorio->banner) }}')">
            <div class="center">
                <div class="box">
                    <h3>{{ trans('frontend.escritorio.quem-somos') }}</h3>
                    {!! Tools::traducao($escritorio, 'quem_somos') !!}
                </div>
            </div>
        </div>

        <div class="center textos">
            <div class="texto">
                <h4>{{ trans('frontend.nav.escritorio') }}</h4>
                {!! Tools::traducao($escritorio, 'o_escritorio') !!}
            </div>

            <div class="missao-visao">
                <h4>{{ trans('frontend.escritorio.missao') }}</h4>
                <p>{{ Tools::traducao($escritorio, 'missao') }}</p>

                <h4>{{ trans('frontend.escritorio.visao') }}</h4>
                <p>{{ Tools::traducao($escritorio, 'visao') }}</p>
            </div>
        </div>

        <div class="premios">
            <div class="center">
                <h4>{{ trans('frontend.escritorio.premios') }}</h4>
                {!! Tools::traducao($escritorio, 'premios') !!}
            </div>
        </div>
    </div>

@endsection
