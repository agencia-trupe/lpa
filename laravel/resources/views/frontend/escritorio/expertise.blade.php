@extends('frontend.common.template')

@section('content')

    <div class="expertise">
        @foreach($expertise as $e)
        <div class="expertise-div">
            <div class="center">
                <div class="texto">
                    @if($expertise->first() == $e)
                    <h2>{{ trans('frontend.nav.expertise') }}</h2>
                    @endif
                    <h3>{{ Tools::traducao($e, 'titulo') }}</h3>
                    {!! Tools::traducao($e, 'texto') !!}
                </div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/expertise/'.$e->imagem) }}" alt="">
                </div>
            </div>
        </div>
        @endforeach
    </div>

@endsection
