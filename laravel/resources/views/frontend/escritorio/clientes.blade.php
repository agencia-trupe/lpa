@extends('frontend.common.template')

@section('content')

    <div class="clientes">
        <div class="center">
            <h2>{{ trans('frontend.nav.clientes') }}</h2>
            <div class="clientes-imagens">
                @foreach($clientes as $cliente)
                <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
                @endforeach
            </div>

            <h2>{{ trans('frontend.nav.depoimentos') }}</h2>
            <div class="depoimentos">
                <div class="grid-sizer"></div>
                <div class="gutter-sizer"></div>
                @foreach($depoimentos as $depoimento)
                <div class="depoimento">
                    <h4>
                        {{ Tools::traducao($depoimento, 'titulo') }}
                        <span>{{ Tools::traducao($depoimento, 'subtitulo') }}</span>
                    </h4>
                    <span class="aspas">″</span>
                    <p>{!! Tools::traducao($depoimento, 'texto') !!}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
