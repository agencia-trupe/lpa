@extends('frontend.common.template')

@section('content')

    <div class="portfolio">
        <div class="center">
            <div class="portfolio-show">
                <div class="abertura">
                    <div class="informacoes">
                        <h2>{{ Tools::traducao($projeto, 'titulo') }}</h2>
                        @if($projeto->cidade)
                        <p>
                            <span>{{ trans('frontend.projetos.cidade') }}</span>
                            <span>{{ Tools::traducao($projeto, 'cidade') }}</span>
                        </p>
                        @endif
                        @if(Tools::traducao($projeto, 'area'))
                        <p>
                            <span>{{ trans('frontend.projetos.area') }}</span>
                            <span>{{ Tools::traducao($projeto, 'area') }}</span>
                        </p>
                        @endif
                        <p>
                            <span>{{ trans('frontend.projetos.ano') }}</span>
                            <span>{{ $projeto->ano }}</span>
                        </p>
                        @if(Tools::traducao($projeto, 'equipe'))
                        <p>
                            <span>{{ trans('frontend.projetos.equipe-arquitetura') }}</span>
                            <span>{{ Tools::traducao($projeto, 'equipe') }}</span>
                        </p>
                        @endif
                        @if(Tools::traducao($projeto, 'creditos_fotografia'))
                        <p>
                            <span>{{ trans('frontend.projetos.creditos-fotografia') }}</span>
                            <span>{{ Tools::traducao($projeto, 'creditos_fotografia') }}</span>
                        </p>
                        @endif
                    </div>
                    <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_1) }}" class="imagem-1" alt="">
                </div>

                @if($projeto->imagem_2)
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_2) }}" class="imagem-2" alt="">
                @endif
                @if($projeto->imagem_3)
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_3) }}" class="imagem-3" alt="">
                @endif
                @if($projeto->imagem_4)
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_4) }}" class="imagem-4" alt="">
                @endif
                @if($projeto->imagem_5)
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_5) }}" class="imagem-5" alt="">
                @endif

                @if(Tools::traducao($projeto, 'descricao') || Tools::traducao($projeto, 'diferenciais_tecnicos'))
                <div class="descricao-diferenciais">
                    @if(Tools::traducao($projeto, 'descricao'))
                    <div class="descricao">
                        <span>{{ Tools::traducao($projeto, 'descricao') }}</span>
                    </div>
                    @endif
                </div>
                @endif

                @if($projeto->imagem_6)
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_6) }}" class="imagem-6" alt="">
                @endif

                @if(Tools::traducao($projeto, 'padroes_de_cores'))
                <div class="padroes">
                    <span>{{ Tools::traducao($projeto, 'padroes_de_cores') }}</span>
                </div>
                @endif

                <div class="conceitos">
                    @if($projeto->imagem_7)
                    <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_7) }}" class="imagem-7" alt="">
                    @endif

                    @if(Tools::traducao($projeto, 'novos_conceitos'))
                    <div class="conceitos-texto">
                        <span>{{ Tools::traducao($projeto, 'novos_conceitos') }}</span>
                    </div>
                    @endif
                </div>

                @if($projeto->imagem_8)
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_8) }}" class="imagem-8" alt="">
                @endif
                @if($projeto->imagem_9)
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_9) }}" class="imagem-9" alt="">
                @endif
                @if($projeto->imagem_10)
                <img src="{{ asset('assets/img/projetos/'.$projeto->imagem_10) }}" class="imagem-10" alt="">
                @endif
            </div>
        </div>
    </div>

@endsection
