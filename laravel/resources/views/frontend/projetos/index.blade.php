@extends('frontend.common.template')

@section('content')

    <div class="portfolio">
        <div class="center">
            <div class="categorias">
                <a href="{{ route('projetos') }}" @if(!$categoria->exists) class="active" @endif>{{ app()->getLocale() === 'pt' ? 'TODOS' : 'ALL' }}</a>
                @foreach($categorias as $cat)
                <a href="{{ route('projetos', $cat->slug) }}" @if($cat->slug == $categoria->slug) class="active" @endif>{{ Tools::traducao($cat, 'titulo') }}</a>
                @endforeach
            </div>

            <div class="portfolio-thumbs">
                @foreach($projetos as $projeto)
                <a href="{{ route('projetos.show', [$projeto->categoria->slug, $projeto->slug]) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/projetos/capa/'.$projeto->capa) }}" alt="">
                    </div>
                    <p>
                        {{ Tools::traducao($projeto, 'titulo') }}
                        <span>{{ $projeto->ano }}</span>
                    </p>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
