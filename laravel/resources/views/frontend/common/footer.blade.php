    <footer>
        <div class="center">
            <div class="links">
                <a href="{{ route('home') }}">HOME</a>
                <a href="{{ route('o-escritorio') }}">{{ trans('frontend.nav.escritorio') }}</a>
                <a href="{{ route('projetos') }}">{{ trans('frontend.nav.projetos') }}</a>
                <a href="{{ route('equipe') }}">{{ trans('frontend.nav.equipe') }}</a>
                <a href="{{ route('blog') }}">{{ trans('frontend.nav.blog') }}</a>
            </div>
            <div class="contato">
                <a href="{{ route('contato') }}">{{ trans('frontend.nav.contato') }}</a>
                <p>{{ $contato->telefone }}</p>
                <p>{!! Tools::traducao($contato, 'endereco') !!}</p>
                <p>
                    @foreach(['facebook', 'instagram', 'linkedin', 'youtube', 'pinterest'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" target="_blank">
                        <img src="{{ asset('assets/img/layout/'.$s.'-icone-branco.png') }}" alt="">
                    </a>
                    @endif
                    @endforeach
                </p>
            </div>
            <div class="copyright">
                <p>
                    <a href="{{ route('politica-de-privacidade') }}">{{ trans('frontend.footer.politica') }}</a>
                </p>
                <p>
                    © {{ date('Y') }} {{ $config->nome_do_site }}
                    <br>{{ trans('frontend.footer.copyright') }}
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.footer.criacao') }}</a>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>