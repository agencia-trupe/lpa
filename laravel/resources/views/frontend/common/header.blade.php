    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>

            <nav id="nav-desktop">
                <div>
                    <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>HOME</a>
                </div>
                <div>
                    <a href="{{ route('o-escritorio') }}" @if(Tools::isActive('o-escritorio*')) class="active" @endif>{{ trans('frontend.nav.escritorio') }}</a>
                    @if(Tools::isActive('o-escritorio*'))
                    <div class="sub">
                        <a href="{{ route('o-escritorio.expertise') }}" @if(Tools::isActive('o-escritorio.expertise')) class="active" @endif>{{ trans('frontend.nav.expertise') }}</a>
                        <a href="{{ route('o-escritorio.clientes') }}" @if(Tools::isActive('o-escritorio.clientes')) class="active" @endif>{{ trans('frontend.nav.clientes') }}</a>
                    </div>
                    @endif
                </div>
                <div>
                    <a href="{{ route('projetos') }}" @if(Tools::isActive('projetos*')) class="active" @endif>{{ trans('frontend.nav.projetos') }}</a>
                </div>
                <div>
                    <a href="{{ route('equipe') }}" @if(Tools::isActive('equipe')) class="active" @endif>{{ trans('frontend.nav.equipe') }}</a>
                </div>
                <div>
                    <a href="{{ route('blog') }}" @if(Tools::isActive('blog*')) class="active" @endif>{{ trans('frontend.nav.blog') }}</a>
                </div>
                @unless(app()->getLocale() == 'en')
                <div>
                    <a href="{{ route('ebook.index') }}" @if(Tools::isActive('ebook*')) class="active" @endif>E-BOOK</a>
                </div>
                @endunless
                <div>
                    <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>{{ trans('frontend.nav.contato') }}</a>
                </div>
            </nav>

            <div class="social">
                @foreach(['facebook', 'instagram', 'linkedin', 'youtube', 'pinterest'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" target="_blank">
                        <img src="{{ asset('assets/img/layout/'.$s.'-icone.png') }}" alt="">
                    </a>
                    @endif
                @endforeach
            </div>

            <?php $lang = app()->getLocale() == 'pt' ? 'en' : 'pt'; ?>
            <a href="{{ route('lang', $lang) }}" class="lang">
                <img src="{{ asset('assets/img/layout/lang-'.$lang.'.png') }}" alt="">
            </a>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <nav id="nav-mobile">
            <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>HOME</a>
            <a href="{{ route('o-escritorio') }}" @if(Tools::isActive('o-escritorio*')) class="active" @endif>{{ trans('frontend.nav.escritorio') }}</a>
            <div class="sub">
                <a href="{{ route('o-escritorio.expertise') }}" @if(Tools::isActive('o-escritorio.expertise')) class="active" @endif>{{ trans('frontend.nav.expertise') }}</a>
                <a href="{{ route('o-escritorio.clientes') }}" @if(Tools::isActive('o-escritorio.clientes')) class="active" @endif>{{ trans('frontend.nav.clientes') }}</a>
            </div>
            <a href="{{ route('projetos') }}" @if(Tools::isActive('projetos*')) class="active" @endif>{{ trans('frontend.nav.projetos') }}</a>
            <a href="{{ route('equipe') }}" @if(Tools::isActive('equipe')) class="active" @endif>{{ trans('frontend.nav.equipe') }}</a>
            <a href="{{ route('blog') }}" @if(Tools::isActive('blog*')) class="active" @endif>{{ trans('frontend.nav.blog') }}</a>
            <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>{{ trans('frontend.nav.contato') }}</a>
        </nav>
    </header>
