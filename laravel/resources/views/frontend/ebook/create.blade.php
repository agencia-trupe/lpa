@extends('frontend.common.template')

@section('ebookPage')

<div class="ebook-page">

    <div class="ebook-center">
        <div class="logo-ebook">
            <a href="{{ route('ebook.index') }}" class="logo">
                <img src="{{ asset('assets/img/layout/marca-lpa.svg') }}" alt="Logo LPA">
            </a>
        </div>

        <div class="informacoes-ebook">
            <p class="boas-vindas">{{ $dadosPagina->texto_inicio }}</p>
            <p class="institucional-pt1">{{ $dadosPagina->texto_parte1 }}</p>
            <p class="institucional-pt2">{{ $dadosPagina->texto_parte2 }}</p>
            @foreach($dadosArquivo as $arquivo)
            <div class="ebook-dados">
                <img src="{{ asset('assets/img/layout/icone-ebook.svg') }}" alt="">
                <p class="descricao"><strong>{{ $arquivo->titulo }}</strong> ({{ $arquivo->prefixo}}) - {{ $arquivo->descricao }}</p>
            </div>
            @endforeach
        </div>

        <div class="form-ebook">
            <p class="institucional-pt3">{{ $dadosPagina->texto_download }}</p>
            <p class="msg-final">{{ $dadosPagina->texto_final }}</p>


            <form class="ebook-cadastro" action="{{ route('ebook.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                @if($errors->any())
                <p class="ebook-form-response erro">
                    {{$errors->first()}}
                </p>
                @endif
                
                <input type="hidden" name="arquivo_id" value="{{ $arquivoSlug->id }}">
                <input type="text" name="nome" value="{{ old('nome') }}" placeholder="Nome" required>
                <input type="email" name="email" value="{{ old('email') }}" placeholder="E-mail" required>
                <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="Telefone" required>
                <input type="text" name="empresa" value="{{ old('empresa') }}" placeholder="Nome da Empresa" required>
                <button class="form-download" type="submit">
                    <p class="titulo">Obter Documento</p>
                    <div class="icon">
                        <img src="{{ asset('assets/img/layout/icone-download.svg') }}" alt="">
                    </div>
                </button>
            </form>
        </div>

        <div class="bola-verde"></div>

    </div>

    <div class="ebook-footer">
        <a href="{{ route('home') }}" class="visite-site" type="submit">Visite o site da LPA Arquitetura: www.lpa.arq.br</a>
        <p class="direitos">© {{ date('Y') }} {{ $config->nome_do_site }} - Todos os direitos reservados.</p>
    </div>
</div>

@endsection