@extends('frontend.common.template')

@section('ebookPage')

<div class="ebook-page">

    <div class="ebook-center">
        <div class="logo-ebook">
            <a href="{{ route('ebook.index') }}" class="logo">
                <img src="{{ asset('assets/img/layout/marca-lpa.svg') }}" alt="Logo LPA">
            </a>
        </div>

        <div class="informacoes-ebook">
            <p class="boas-vindas">{{ $dadosPagina->texto_inicio }}</p>
            <p class="institucional-pt1">{{ $dadosPagina->texto_parte1 }}</p>
            <p class="institucional-pt2">{{ $dadosPagina->texto_parte2 }}</p>
            @foreach($dadosArquivo as $arquivo)
            <div class="ebook-dados">
                <img src="{{ asset('assets/img/layout/icone-ebook.svg') }}" alt="">
                <p class="descricao"><strong>{{ $arquivo->titulo }}</strong> ({{ $arquivo->prefixo}}) - {{ $arquivo->descricao }}</p>
            </div>
            @endforeach
        </div>

        <div class="form-ebook">
            <p class="institucional-pt3">{{ $dadosPagina->texto_download }}</p>
            <p class="msg-final">{{ $dadosPagina->texto_final }}</p>

            <div class="msg-download">
                <p>Agradecemos o interesse.</p>
                <p>O e-book foi enviado no seu e-mail.</p>
                <a href="{{ route('ebook.index') }}"><< Retornar</a>
            </div>
        </div>

        <div class="bola-verde"></div>

    </div>

    <div class="ebook-footer">
        <a href="{{ route('home') }}" class="visite-site" type="submit">Visite o site da LPA Arquitetura: www.lpa.arq.br</a>
        <p class="direitos">© {{ date('Y') }} {{ $config->nome_do_site }} - Todos os direitos reservados.</p>
    </div>
</div>

@endsection