@extends('frontend.common.template')

@section('content')

    <div class="portfolio">
        <div class="center">
            <div class="portfolio-show">
                <div class="abertura">
                    <div class="informacoes">
                        <h2>{{ Tools::traducao($estudo, 'titulo') }}</h2>
                        @if($estudo->cidade)
                        <p>
                            <span>{{ trans('frontend.projetos.cidade') }}</span>
                            <span>{{ Tools::traducao($estudo, 'cidade') }}</span>
                        </p>
                        @endif
                        @if(Tools::traducao($estudo, 'area'))
                        <p>
                            <span>{{ trans('frontend.projetos.area') }}</span>
                            <span>{{ Tools::traducao($estudo, 'area') }}</span>
                        </p>
                        @endif
                        <p>
                            <span>{{ trans('frontend.projetos.ano') }}</span>
                            <span>{{ $estudo->ano }}</span>
                        </p>
                        @if(Tools::traducao($estudo, 'equipe'))
                        <p>
                            <span>{{ trans('frontend.projetos.equipe-arquitetura') }}</span>
                            <span>{{ Tools::traducao($estudo, 'equipe') }}</span>
                        </p>
                        @endif
                    </div>
                    <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_1) }}" class="imagem-1" alt="">
                </div>

                @if($estudo->imagem_2)
                <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_2) }}" class="imagem-2" alt="">
                @endif
                @if($estudo->imagem_3)
                <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_3) }}" class="imagem-3" alt="">
                @endif
                @if($estudo->imagem_4)
                <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_4) }}" class="imagem-4" alt="">
                @endif
                @if($estudo->imagem_5)
                <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_5) }}" class="imagem-5" alt="">
                @endif

                @if(Tools::traducao($estudo, 'descricao') || Tools::traducao($estudo, 'diferenciais_tecnicos'))
                <div class="descricao-diferenciais">
                    @if(Tools::traducao($estudo, 'descricao'))
                    <div class="descricao">
                        <span>{{ trans('frontend.projetos.descricao') }}:</span>
                        <span>{{ Tools::traducao($estudo, 'descricao') }}</span>
                    </div>
                    @endif

                    @if(Tools::traducao($estudo, 'diferenciais_tecnicos'))
                    <div class="diferenciais">
                        <span>{{ trans('frontend.projetos.diferenciais') }}</span>
                        <span>{!! Tools::traducao($estudo, 'diferenciais_tecnicos') !!}</span>
                    </div>
                    @endif
                </div>
                @endif

                @if($estudo->imagem_6)
                <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_6) }}" class="imagem-6" alt="">
                @endif

                @if(Tools::traducao($estudo, 'padroes_de_cores'))
                <div class="padroes">
                    <span>{{ trans('frontend.projetos.padroes') }}</span>
                    <span>{{ Tools::traducao($estudo, 'padroes_de_cores') }}</span>
                </div>
                @endif

                <div class="conceitos">
                    @if($estudo->imagem_7)
                    <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_7) }}" class="imagem-7" alt="">
                    @endif

                    @if(Tools::traducao($estudo, 'novos_conceitos'))
                    <div class="conceitos-texto">
                        <span>{{ trans('frontend.projetos.conceitos') }}</span>
                        <span>{{ Tools::traducao($estudo, 'novos_conceitos') }}</span>
                    </div>
                    @endif
                </div>

                @if($estudo->imagem_8)
                <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_8) }}" class="imagem-8" alt="">
                @endif
                @if($estudo->imagem_9)
                <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_9) }}" class="imagem-9" alt="">
                @endif
                @if($estudo->imagem_10)
                <img src="{{ asset('assets/img/estudos/'.$estudo->imagem_10) }}" class="imagem-10" alt="">
                @endif
            </div>
        </div>
    </div>

@endsection
