@extends('frontend.common.template')

@section('content')

    <div class="portfolio">
        <div class="center">
            <div class="portfolio-thumbs">
                @foreach($estudos as $estudo)
                <a href="{{ route('portfolio.estudos.show', $estudo->slug) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/estudos/capa/'.$estudo->capa) }}" alt="">
                    </div>
                    <p>
                        {{ Tools::traducao($estudo, 'titulo') }}
                        <span>{{ $estudo->ano }}</span>
                    </p>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
