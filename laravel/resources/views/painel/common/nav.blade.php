<ul class="nav navbar-nav">
	<li @if(str_is('painel.destaques*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.destaques.index') }}">Destaques</a>
	</li>
    <li @if(str_is('painel.escritorio*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.escritorio.index') }}">Escritório</a>
    </li>
	<li @if(str_is('painel.projetos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.projetos.index') }}">Projetos</a>
	</li>
	{{--<li @if(str_is('painel.estudos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.estudos.index') }}">Estudos</a>
	</li>--}}
	<li @if(str_is('painel.expertise*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.expertise.index') }}">Expertise</a>
	</li>
	<li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.clientes.index') }}">Clientes</a>
	</li>
	<li @if(str_is('painel.depoimentos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.depoimentos.index') }}">Depoimentos</a>
	</li>
	<li @if(str_is('painel.equipe*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.equipe.index') }}">Equipe</a>
	</li>
	<li @if(str_is('painel.blog*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.blog.index') }}">Blog</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if(($contatosNaoLidos + $ouvidoriaNaoLidos) >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos + $ouvidoriaNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.contato.index', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li @if(str_is('painel.contato.recebidos.*', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li @if(str_is('painel.contato.ouvidoria.*', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.contato.ouvidoria.index') }}">
                Ouvidoria
                @if($ouvidoriaNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $ouvidoriaNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            E-book
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.ebook.pagina.index', Route::currentRouteName())) class="active" @endif><a href="{{ route('ebook.pagina.index') }}">Pagina</a></li>
            <li @if(str_is('painel.ebook.arquivos.*', Route::currentRouteName())) class="active" @endif><a href="{{ route('ebook.arquivos.index') }}">Arquivos</a></li>
            <li @if(str_is('painel.ebook.downloads.*', Route::currentRouteName())) class="active" @endif><a href="{{ route('ebook.downloads') }}">Downloads</a></li>
        </ul>
    </li>
</ul>
