@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Escritório</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.escritorio.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.escritorio.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
