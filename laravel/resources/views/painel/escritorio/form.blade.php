@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('banner', 'Banner') !!}
    @if($registro->banner)
    <img src="{{ url('assets/img/escritorio/'.$registro->banner) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('banner', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('quem_somos', 'Quem Somos') !!}
    {!! Form::textarea('quem_somos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('quem_somos_en', 'Quem Somos [INGLÊS]') !!}
    {!! Form::textarea('quem_somos_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('o_escritorio', 'O Escritório') !!}
    {!! Form::textarea('o_escritorio', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('o_escritorio_en', 'O Escritório [INGLÊS]') !!}
    {!! Form::textarea('o_escritorio_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('missao', 'Missão') !!}
            {!! Form::textarea('missao', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('missao_en', 'Missão [INGLÊS]') !!}
            {!! Form::textarea('missao_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('visao', 'Visão') !!}
            {!! Form::textarea('visao', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('visao_en', 'Visão [INGLÊS]') !!}
            {!! Form::textarea('visao_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('premios', 'Prêmios') !!}
    {!! Form::textarea('premios', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('premios_en', 'Prêmios [INGLÊS]') !!}
    {!! Form::textarea('premios_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
