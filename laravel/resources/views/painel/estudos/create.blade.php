@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Estudos /</small> Adicionar Estudo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.estudos.store', 'files' => true]) !!}

        @include('painel.estudos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
