@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Estudos /</small> Editar Estudo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.estudos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.estudos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
