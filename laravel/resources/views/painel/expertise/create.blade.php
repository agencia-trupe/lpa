@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Expertise /</small> Adicionar Expertise</h2>
    </legend>

    {!! Form::open(['route' => 'painel.expertise.store', 'files' => true]) !!}

        @include('painel.expertise.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
