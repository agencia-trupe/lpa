@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto [PORTUGUÊS]') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textoPolitica']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto [INGLÊS]') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textoPolitica']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}