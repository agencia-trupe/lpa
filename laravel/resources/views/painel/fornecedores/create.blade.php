@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos / Fornecedores / {{ $projeto->titulo }} /</small> Adicionar Fornecedor</h2>
    </legend>

    {!! Form::open(['route' => ['painel.projetos.fornecedores.store', $projeto->id], 'files' => true]) !!}

        @include('painel.fornecedores.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
