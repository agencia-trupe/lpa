@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos / Fornecedores / {{ $projeto->titulo }} /</small> Editar Fornecedor</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.projetos.fornecedores.update', $projeto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.fornecedores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
