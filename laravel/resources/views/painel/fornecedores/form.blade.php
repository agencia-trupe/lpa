@include('painel.common.flash')

{!! Form::hidden('projetos_id', $projeto->id) !!}

<div class="form-group">
    {!! Form::label('material', 'Material') !!}
    {!! Form::text('material', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('material_en', 'Material [INGLÊS]') !!}
    {!! Form::text('material_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('fornecedor', 'Fornecedor') !!}
    {!! Form::text('fornecedor', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('fornecedor_en', 'Fornecedor [INGLÊS]') !!}
    {!! Form::text('fornecedor_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('detalhes', 'Detalhes') !!}
    {!! Form::text('detalhes', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('detalhes_en', 'Detalhes [INGLÊS]') !!}
    {!! Form::text('detalhes_en', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.fornecedores.index', $projeto->id) }}" class="btn btn-default btn-voltar">Voltar</a>
