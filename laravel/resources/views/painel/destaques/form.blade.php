@include('painel.common.flash')

<p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
    <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
    Selecione apenas um dos campos abaixo.
</p>

<div class="form-group">
    {!! Form::label('projeto', 'Projeto') !!}
    {!! Form::select('projeto', $projetos, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('post', 'Post') !!}
    {!! Form::select('post', $posts, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.destaques.index') }}" class="btn btn-default btn-voltar">Voltar</a>
