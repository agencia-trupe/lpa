@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos / Equipe / {{ $projeto->titulo }} /</small> Adicionar Equipe</h2>
    </legend>

    {!! Form::open(['route' => ['painel.projetos.equipe.store', $projeto->id], 'files' => true]) !!}

        @include('painel.equipe_projetos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
