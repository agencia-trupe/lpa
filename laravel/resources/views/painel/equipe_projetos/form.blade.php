@include('painel.common.flash')

{!! Form::hidden('projetos_id', $projeto->id) !!}

<div class="form-group">
    {!! Form::label('categoria', 'Categoria') !!}
    {!! Form::text('categoria', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('categoria_en', 'Categoria [INGLÊS]') !!}
    {!! Form::text('categoria_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('equipe', 'Equipe') !!}
    {!! Form::text('equipe', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('equipe_en', 'Equipe [INGLÊS]') !!}
    {!! Form::text('equipe_en', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.equipe.index', $projeto->id) }}" class="btn btn-default btn-voltar">Voltar</a>
