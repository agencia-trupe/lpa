@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos / Equipe / {{ $projeto->titulo }} /</small> Editar Equipe</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.projetos.equipe.update', $projeto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.equipe_projetos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
