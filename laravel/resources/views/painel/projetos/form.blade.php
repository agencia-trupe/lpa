@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('projetos_categoria_id', 'Categoria') !!}
    {!! Form::select('projetos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título [INGLÊS]') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa (280x280px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/capa/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade', 'Cidade') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade_en', 'Cidade [INGLÊS]') !!}
    {!! Form::text('cidade_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('area', 'Área') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('area_en', 'Área [INGLÊS]') !!}
    {!! Form::text('area_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('ano', 'Ano') !!}
    {!! Form::text('ano', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('equipe', 'Equipe') !!}
    {!! Form::text('equipe', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('equipe_en', 'Equipe [INGLÊS]') !!}
    {!! Form::text('equipe_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('creditos_fotografia', 'Créditos Fotografia') !!}
    {!! Form::text('creditos_fotografia', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('creditos_fotografia_en', 'Créditos Fotografia [INGLÊS]') !!}
    {!! Form::text('creditos_fotografia_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('descricao', 'Descrição') !!}
            {!! Form::textarea('descricao', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição [INGLÊS]') !!}
            {!! Form::textarea('descricao_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('padroes_de_cores', 'Padrões de cores') !!}
            {!! Form::textarea('padroes_de_cores', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('padroes_de_cores_en', 'Padrões de cores [INGLÊS]') !!}
            {!! Form::textarea('padroes_de_cores_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('novos_conceitos', 'Novos conceitos') !!}
            {!! Form::textarea('novos_conceitos', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('novos_conceitos_en', 'Novos conceitos [INGLÊS]') !!}
            {!! Form::textarea('novos_conceitos_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1 (705x465px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_2', 'Imagem 2 (705x540px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_2)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 2]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_3', 'Imagem 3 (465x310px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_3)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 3]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_4', 'Imagem 4 (465x310px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_4)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 4]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_5', 'Imagem 5 (705x465px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_5)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 5]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_6', 'Imagem 6 (1200x800px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_6)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 6]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_6) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_6', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_7', 'Imagem 7 (705x465px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_7)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 7]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_7) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_7', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_8', 'Imagem 8 (465x700px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_8)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 8]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_8) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_8', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_9', 'Imagem 9 (465x310px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_9)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 9]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_9) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_9', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_10', 'Imagem 10 (705x465px)') !!}
@if($submitText == 'Alterar' && $registro->imagem_10)
    <div style="margin-bottom:5px">
        <a href="{{ route('projetos.remover-imagem', [$registro->id, 10]) }}" class="label label-danger btn-delete btn-delete-link"><span class="glyphicon glyphicon-remove" style="margin-right:5px"></span>excluir imagem</a>
    </div>
    <img src="{{ url('assets/img/projetos/'.$registro->imagem_10) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_10', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
