@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>Cadastros para download de e-books</h2>
</legend>

@if(!count($cadastros))
<div class="alert alert-warning" role="alert">Nenhum cadastro para download recebido.</div>
@else
<table class="table table-striped table-bordered table-hover data-table">
    <thead>
        <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Telefone</th>
            <th>Empresa</th>
            <th>Arquivo</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($cadastros as $cadastro)
        <tr>
            <td data-order="{{ $cadastro->created_at_order }}">{{ $cadastro->created_at->format('d/m/Y h:i') }}</td>
            <td>{{ $cadastro->nome }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $cadastro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $cadastro->email }}
            </td>
            <td>{{ $cadastro->telefone }}</td>
            <td>{{ $cadastro->empresa }}</td>
            <td>{{ $arquivos->find($cadastro->arquivo_id)->titulo }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@if(count($cadastros) != 0)
<div class="exportar-csv">
    <p>Para exportar os cadastros para downloads de e-books, clique aqui:</p>
    <a href="{{ route('ebook.downloads.export') }}">Exportar</a>
</div>
@endif

@endsection