@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>E-book /</small> Editar E-book</h2>
    </legend>

    {!! Form::model($arquivo, [
        'route'  => ['ebook.arquivos.update', $arquivo->id],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.ebook.arquivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
