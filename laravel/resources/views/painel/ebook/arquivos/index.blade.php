@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        <a href="{{ route('ebook.arquivos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar E-book</a>
    </h2>
</legend>

@if(!count($arquivos))
<div class="alert alert-warning" role="alert" style="margin-top:50px;">Nenhum arquivo encontrado.</div>
@else

<h2>E-books Ativos</h2>
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="ebook_arquivo">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Título</th>
            <th>Arquivo</th>
            <th>Status</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($arquivos as $arquivo)
        @if($arquivo->status == 1 || $arquivo->status == true)
        <tr class="tr-row" id="{{ $arquivo->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $arquivo->titulo }}</td>
            <td>{{ $arquivo->arquivo }}</td>
            <td>{{ $status[$arquivo->status] }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['ebook.arquivos.destroy', $arquivo->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('ebook.arquivos.edit', $arquivo->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete-ebook"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>

<h2 class="titulo-inativos">E-books Inativos</h2>
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="ebook_arquivo">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Título</th>
            <th>Arquivo</th>
            <th>Status</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($arquivos as $arquivo)
        @if($arquivo->status == 0 || $arquivo->status == false)
        <tr class="tr-row" id="{{ $arquivo->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $arquivo->titulo }}</td>
            <td>{{ $arquivo->arquivo }}</td>
            <td>{{ $status[$arquivo->status] }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['ebook.arquivos.destroy', $arquivo->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('ebook.arquivos.edit', $arquivo->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete-ebook"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
@endif

@endsection