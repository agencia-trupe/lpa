@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>E-book /</small> Adicionar E-book</h2>
    </legend>

    {!! Form::open(['route' => 'ebook.arquivos.store', 'files' => true]) !!}

        @include('painel.ebook.arquivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection