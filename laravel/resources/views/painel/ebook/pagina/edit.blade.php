@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Página E-book</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['ebook.pagina.update', $registro->id],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.ebook.pagina.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection