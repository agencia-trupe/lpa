@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_inicio', 'Texto inicial') !!}
    {!! Form::text('texto_inicio', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_parte1', 'Texto institucional - parte 1') !!}
    {!! Form::textarea('texto_parte1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_parte2', 'Texto institucional - parte 2') !!}
    {!! Form::textarea('texto_parte2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_download', 'Texto download') !!}
    {!! Form::textarea('texto_download', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_final', 'Texto final') !!}
    {!! Form::text('texto_final', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}