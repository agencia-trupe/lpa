<!DOCTYPE html>
<html>
<head>
    <title>[EBOOK] {{ $config->nome_do_site }}</title>
    <meta charset="utf-8">
</head>
<body>
    
    <p style='font-weight:bold;font-size:20px;font-family:Verdana;'>Olá, {{ $nome }}</p>
    <br>
    <p style='font-weight:500;font-size:16px;font-family:Verdana;'>O seu e-book está pronto para download:</p>
    <p style='font-weight:500;font-size:16px;font-family:Verdana;'>Clique para baixar o: <a href="{{ $link }}" target="_blank" style='font-weight:400;font-size:16px;font-family:Verdana;'>{{ $ebook->titulo }}</a></p>
    <br>
    <br>
    <p style='font-weight:500;font-size:16px;font-family:Verdana;'>{{ $config->nome_do_site }}</p>
    <a href="{{ route('home') }}" target="_blank" style='font-weight:400;font-size:16px;font-family:Verdana;'>www.lpa.arq.br</a>

</body>
</html>
