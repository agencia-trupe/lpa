import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.blog .imagens a').fancybox();

$(document).on('submit', '#form-contato', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato',
        data: {
            nome: $('#nome').val(),
            email: $('#email').val(),
            mensagem: $('#mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            if (data.responseJSON) {
                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
            }
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});

$(document).on('submit', '#form-ouvidoria', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-ouvidoria-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/ouvidoria',
        data: {
            nome: $('#nome_o').val(),
            email: $('#email_o').val(),
            mensagem: $('#mensagem_o').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            if (data.responseJSON) {
                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
            }
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});

$('.depoimentos').masonry({
    columnWidth: '.grid-sizer',
    gutter: '.gutter-sizer',
    itemSelector: '.depoimento',
    percentPosition: true
});

// AVISO DE COOKIES
$(document).ready(function() {
    $('.aviso-cookies').hide();

    if (window.location.href == routeHome) {
        $('.aviso-cookies').show();

        $('.aceitar-cookies').click(function() {
            var url = window.location.origin + '/aceite-de-cookies';

            $.ajax({
                type: 'POST',
                url: url,
                success: function(data, textStatus, jqXHR) {
                    $('.aviso-cookies').hide();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                },
            });
        });
    }
});
