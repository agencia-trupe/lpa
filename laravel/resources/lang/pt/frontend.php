<?php

return [

    'nav' => [
        'escritorio'  => 'O ESCRITÓRIO',
        'expertise'   => 'EXPERTISE',
        'clientes'    => 'CLIENTES',
        'depoimentos' => 'DEPOIMENTOS',
        'portfolio'   => 'PORTFOLIO',
        'projetos'    => 'PROJETOS',
        'estudos'     => 'ESTUDOS',
        'equipe'      => 'EQUIPE',
        'blog'        => 'BLOG',
        'contato'     => 'CONTATO'
    ],

    'footer' => [
        'copyright' => 'TODOS OS DIREITOS RESERVADOS',
        'criacao'   => 'Criação de sites:',
        'politica'  => 'POLÍTICA DE PRIVACIDADE'
    ],

    'escritorio' => [
        'quem-somos' => 'QUEM SOMOS',
        'missao'     => 'MISSÃO',
        'visao'      => 'VISÃO',
        'premios'    => 'PRÊMIOS'
    ],

    'projetos' => [
        'cidade'              => 'cidade:',
        'area'                => 'área do projeto:',
        'ano'                 => 'ano:',
        'equipe-arquitetura'  => 'equipe de arquitetura:',
        'creditos-fotografia' => 'créditos fotografia:',
        'descricao'           => 'Descrição do projeto:',
        'diferenciais'        => 'DIFERENCIAIS TÉCNICOS DO PROJETO:',
        'padroes'             => 'Padrões de cores:',
        'conceitos'           => 'Novos conceitos:',
        'equipe-projetistas'  => 'EQUIPE DE PROJETISTAS E CONSULTORES',
        'fornecedores'        => 'OUTROS FORNECEDORES',
    ],

    'contato' => [
        'contate'   => 'CONTATE-NOS',
        'nome'      => 'nome',
        'mensagem'  => 'mensagem',
        'enviar'    => 'ENVIAR',
        'sucesso'   => 'Mensagem enviada com sucesso!',
        'erro'      => 'Preencha todos os campos corretamente.',
        'ouvidoria' => 'OUVIDORIA',
        'opcional'  => '(opcional)',
    ],

    'cookies' => [
        'texto1'   => 'Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa ',
        'politica' => 'Política de Privacidade',
        'texto2'   => ' e saiba mais.',
        'aceitar'  => 'ACEITAR E FECHAR',
    ],

];
