<?php

return [

    'nav' => [
        'escritorio'  => 'OUR OFFICE',
        'expertise'   => 'EXPERTISE',
        'clientes'    => 'OUR CLIENTS',
        'depoimentos' => 'TESTIMONIALS',
        'portfolio'   => 'PORTFOLIO',
        'projetos'    => 'PROJECTS',
        'estudos'     => 'ESTUDOS',
        'equipe'      => 'OUR TEAM',
        'blog'        => 'BLOG',
        'contato'     => 'CONTACT'
    ],

    'footer' => [
        'copyright' => 'ALL RIGHTS RESERVED',
        'criacao'   => 'Sites:',
        'politica'  => 'PRIVACY POLICY'
    ],

    'escritorio' => [
        'quem-somos' => 'WHO WE ARE',
        'missao'     => 'OUR MISSION',
        'visao'      => 'OUR VISION',
        'premios'    => 'OUR PRIZES'
    ],

    'projetos' => [
        'cidade'              => 'city:',
        'area'                => 'project area:',
        'ano'                 => 'year:',
        'equipe-arquitetura'  => 'architecture team:',
        'creditos-fotografia' => 'photo credits:',
        'descricao'           => 'Project description:',
        'diferenciais'        => 'TECHNICAL DIFFERENTIALS OF THE PROJECT:',
        'padroes'             => 'Color Patterns:',
        'conceitos'           => 'New concepts:',
        'equipe-projetistas'  => 'PROJETIST TEAM AND CONSULTANTS',
        'fornecedores'        => 'OTHER SUPPLIERS',
    ],

    'contato' => [
        'contate'   => 'CONTACT US',
        'nome'      => 'name',
        'mensagem'  => 'message',
        'enviar'    => 'SEND',
        'sucesso'   => 'Message sent successfully!',
        'erro'      => 'Fill all fields correctly.',
        'ouvidoria' => 'OMBUDSMAN',
        'opcional'  => '(optional)',
    ],

    'cookies' => [
        'texto1'   => 'We use cookies to personalize content, track ads and offer you a safer browsing experience. By continuing to browse our site you agree to the use of this information. Read our ',
        'politica' => 'Privacy Policy',
        'texto2'   => ' and learn more.',
        'aceitar'  => 'ACCEPT AND CLOSE',
    ],

];
