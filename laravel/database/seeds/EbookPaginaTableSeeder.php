<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EbookPaginaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('ebook_pagina')->insert([
            'texto_inicio'      => 'Olá!',
            'texto_parte1'      => 'Nossas vidas mudaram do dia para a noite com a pandemia do COVID-19. Sem muita ideia de como fazer essas mudanças, nos reinventamos para conseguir conciliar a vida pessoal, profissional e acadêmica dentro das nossas casas.',
            'texto_parte2'      => 'Com o objetivo de ajudar as empresas no retorno aos escritórios, a LPA criou o Guia do Escritório pós COVID-19, dividido em dois volumes.',
            'texto_download'    => 'Para fazer o download do é muito simples! Escolha o arquivo abaixo e nós enviaremos o PDF para você!',
            'texto_final'       => 'Esperamos ter ajudado!',
        ]);
    }
}
