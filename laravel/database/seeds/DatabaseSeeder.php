<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(EscritorioSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(EbookPaginaTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);

        Model::reguard();
    }
}
