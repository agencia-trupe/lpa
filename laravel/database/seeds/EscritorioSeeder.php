<?php

use Illuminate\Database\Seeder;

class EscritorioSeeder extends Seeder
{
    public function run()
    {
        DB::table('escritorio')->insert([
            'banner' => '',
            'quem_somos' => '',
            'o_escritorio' => '',
            'missao' => '',
            'visao' => '',
            'premios' => '',
        ]);
    }
}
