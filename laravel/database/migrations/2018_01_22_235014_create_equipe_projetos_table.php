<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipeProjetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipe_projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projetos_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('categoria');
            $table->string('categoria_en');
            $table->string('equipe');
            $table->string('equipe_en');
            $table->foreign('projetos_id')->references('id')->on('projetos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipe_projetos');
    }
}
