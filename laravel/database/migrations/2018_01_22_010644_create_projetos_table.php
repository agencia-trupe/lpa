<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosTable extends Migration
{
    public function up()
    {
        Schema::create('projetos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projetos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('capa');
            $table->string('cidade');
            $table->string('cidade_en');
            $table->string('area');
            $table->string('area_en');
            $table->string('ano');
            $table->text('equipe');
            $table->text('equipe_en');
            $table->text('creditos_fotografia');
            $table->text('creditos_fotografia_en');
            $table->text('descricao');
            $table->text('descricao_en');
            $table->text('diferenciais_tecnicos');
            $table->text('diferenciais_tecnicos_en');
            $table->text('padroes_de_cores');
            $table->text('padroes_de_cores_en');
            $table->text('novos_conceitos');
            $table->text('novos_conceitos_en');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->string('imagem_5');
            $table->string('imagem_6');
            $table->string('imagem_7');
            $table->string('imagem_8');
            $table->string('imagem_9');
            $table->string('imagem_10');
            $table->foreign('projetos_categoria_id')->references('id')->on('projetos_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('projetos');
        Schema::drop('projetos_categorias');
    }
}
