<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscritorioTable extends Migration
{
    public function up()
    {
        Schema::create('escritorio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner');
            $table->text('quem_somos');
            $table->text('quem_somos_en');
            $table->text('o_escritorio');
            $table->text('o_escritorio_en');
            $table->text('missao');
            $table->text('missao_en');
            $table->text('visao');
            $table->text('visao_en');
            $table->text('premios');
            $table->text('premios_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('escritorio');
    }
}
