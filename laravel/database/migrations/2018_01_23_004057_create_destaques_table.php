<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestaquesTable extends Migration
{
    public function up()
    {
        Schema::create('destaques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('projeto')->unsigned()->nullable();
            $table->foreign('projeto')->references('id')->on('projetos')->onDelete('cascade');
            $table->integer('post')->unsigned()->nullable();
            $table->foreign('post')->references('id')->on('blog')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('destaques');
    }
}
