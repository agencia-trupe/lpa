<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEbookCadastro extends Migration
{
    public function up()
    {
        Schema::create('ebook_cadastro', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('empresa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ebook_cadastro');
    }
}
