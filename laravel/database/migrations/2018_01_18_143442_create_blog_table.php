<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->text('chamada');
            $table->text('chamada_en');
            $table->string('capa');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('slug');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('blog');
    }
}
