<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddFkToEbookCadastro extends Migration
{
    public function up()
    {
        Schema::table('ebook_cadastro', function (Blueprint $table) {
            $table->integer('arquivo_id')->unsigned()->after('telefone');
            $table->foreign('arquivo_id')->references('id')->on('ebook_arquivo')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('ebook_cadastro', function (Blueprint $table) {
            $table->dropForeign(['arquivo_id']);
        });
    }
}
