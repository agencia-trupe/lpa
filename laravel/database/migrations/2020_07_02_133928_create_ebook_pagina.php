<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEbookPagina extends Migration
{
    public function up()
    {
        Schema::create('ebook_pagina', function (Blueprint $table) {
            $table->increments('id');
            $table->string('texto_inicio');
            $table->string('texto_parte1');
            $table->string('texto_parte2');
            $table->string('texto_download');
            $table->string('texto_final');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ebook_pagina');
    }
}
