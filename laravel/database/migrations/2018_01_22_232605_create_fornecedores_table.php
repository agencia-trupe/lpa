<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresTable extends Migration
{
    public function up()
    {
        Schema::create('fornecedores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projetos_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('material');
            $table->string('material_en');
            $table->string('fornecedor');
            $table->string('fornecedor_en');
            $table->string('detalhes');
            $table->string('detalhes_en');
            $table->foreign('projetos_id')->references('id')->on('projetos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('fornecedores');
    }
}
