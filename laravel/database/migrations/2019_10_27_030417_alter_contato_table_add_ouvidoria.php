<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContatoTableAddOuvidoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contato', function (Blueprint $table) {
            $table->text('ouvidoria')->after('pinterest');
            $table->text('ouvidoria_en')->after('ouvidoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contato', function (Blueprint $table) {
            $table->dropColumn('ouvidoria');
            $table->dropColumn('ouvidoria_en');
        });
    }
}
