--
-- Table structure for table `ebook_arquivo`
--

DROP TABLE IF EXISTS `ebook_arquivo`;

CREATE TABLE `ebook_arquivo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prefixo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ebook_arquivo`
--

LOCK TABLES `ebook_arquivo` WRITE;

INSERT INTO `ebook_arquivo` VALUES (1,0,'retornando-ao-escritorio','Retornando ao escritório','Volume 1','Retornando ao Escritório contém dicas para auxiliar as empresas com a criação de um plano de ação, focado em medidas mais diretas que podem ser colocadas em prática facilmente.','LPA-GUIA-DO-ESCRITORIO-POS-COVID-19-V1.pdf',NULL,NULL),(2,1,'recriando-o-escritorio','Recriando o escritório','Volume 2','Recriando o Escritório contém dicas para a criação de um plano de reconfiguração que poderá ser efetuado em um momento futuro, pós-crise, também visando a saúde, a segurança e o bem-estar de todos.','LPA-GUIA-DO-ESCRITORIO-POS-COVID-19-V2.pdf',NULL,NULL);

UNLOCK TABLES;

--
-- Table structure for table `ebook_cadastro`
--

DROP TABLE IF EXISTS `ebook_cadastro`;

CREATE TABLE `ebook_cadastro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arquivo_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ebook_cadastro_arquivo_id_foreign` (`arquivo_id`),
  CONSTRAINT `ebook_cadastro_arquivo_id_foreign` FOREIGN KEY (`arquivo_id`) REFERENCES `ebook_arquivo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `ebook_pagina`
--

DROP TABLE IF EXISTS `ebook_pagina`;

CREATE TABLE `ebook_pagina` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_boas_vindas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_institucional_pt1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_institucional_pt2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_institucional_pt3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_msg_final` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ebook_pagina`
--

LOCK TABLES `ebook_pagina` WRITE;

INSERT INTO `ebook_pagina` VALUES (1,'Olá!','Nossas vidas mudaram do dia para a noite com a pandemia do COVID-19. Sem muita ideia de como fazer essas mudanças, nos reinventamos para conseguir conciliar a vida pessoal, profissional e acadêmica dentro das nossas casas.','Com o objetivo de ajudar as empresas no retorno aos escritórios, a LPA criou o Guia do Escritório pós COVID-19, dividido em dois volumes.','Para fazer o download do Volume 1, é muito simples! Escolha o arquivo abaixo e nós enviaremos o PDF para você! Em breve, você também poderá baixar o Volume 2.','Esperamos ter ajudado!',NULL,NULL);

UNLOCK TABLES;

--
-- Dump completed on 2020-07-07 10:18:14
