--
-- Table structure for table `ebook_arquivo`
--

ALTER TABLE `ebook_arquivo` ADD `status` tinyint(1) NOT NULL;

UPDATE `ebook_arquivo` SET `status` = 1;

--
-- Table structure for table `ebook_cadastro`
--

DROP TABLE IF EXISTS `ebook_cadastro`;

CREATE TABLE `ebook_cadastro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ebook_cadastro_arquivo_id_foreign` (`arquivo_id`),
  CONSTRAINT `ebook_cadastro_arquivo_id_foreign` FOREIGN KEY (`arquivo_id`) REFERENCES `ebook_arquivo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ebook_cadastro`
--

LOCK TABLES `ebook_cadastro` WRITE;

INSERT INTO `ebook_cadastro` VALUES 
	(1,'maria luiza ','mluizanapoles@gmail.com','(11) 98316-6554','',2,'2020-07-13 20:37:46','2020-07-13 20:37:46'),
    (2,'maria luiza ','mluizanapoles@gmail.com','(11) 98316-6554','',2,'2020-07-13 20:37:55','2020-07-13 20:37:55'),
    (3,'maria luiza ','mluizanapoles@gmail.com','(11) 98316-6554','',1,'2020-07-13 20:42:11','2020-07-13 20:42:11'),
    (4,'Eneida Nascimento','eneida_14_12@yahoo.com.br','12997113206','',1,'2020-07-14 06:50:36','2020-07-14 06:50:36'),
    (5,'Eneida Nascimento','eneida_14_12@yahoo.com.br','12997113206','',2,'2020-07-14 06:56:45','2020-07-14 06:56:45'),
    (6,'Fernanda','fernanda.cifarelli@saeng.com.br','','',1,'2020-07-15 00:36:28','2020-07-15 00:36:28'),
    (7,'Danielle Leper','danileper@yahoo.com.br','','',1,'2020-07-15 20:05:31','2020-07-15 20:05:31'),
    (8,'Danielle Leper','danileper@yahoo.com.br','','',2,'2020-07-15 20:05:44','2020-07-15 20:05:44'),
    (9,'Menina Triste','ameninatriste.arte@gmail.com','','',1,'2020-07-16 04:03:24','2020-07-16 04:03:24'),
    (10,'Menina Triste','ameninatriste.arte@gmail.com','','',2,'2020-07-16 04:03:39','2020-07-16 04:03:39');

UNLOCK TABLES;

--
-- Table structure for table `ebook_pagina`
--

DROP TABLE IF EXISTS `ebook_pagina`;

CREATE TABLE `ebook_pagina` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_inicio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_parte1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_parte2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_download` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_final` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- Dumping data for table `ebook_pagina`
--

LOCK TABLES `ebook_pagina` WRITE;

INSERT INTO `ebook_pagina` VALUES (1,'Olá!','Nossas vidas mudaram do dia para a noite com a pandemia do COVID-19. Sem muita ideia de como fazer essas mudanças, nos reinventamos para conseguir conciliar a vida pessoal, profissional e acadêmica dentro das nossas casas.','Com o objetivo de ajudar as empresas no retorno aos escritórios, a LPA criou o Guia do Escritório pós COVID-19, dividido em dois volumes.','Para fazer o download do é muito simples! Escolha o arquivo abaixo e nós enviaremos o PDF para você!','Esperamos ter ajudado!',NULL,NULL);

UNLOCK TABLES;

